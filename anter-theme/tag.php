<?php get_header(); ?>
<main role="main">
	<section>
		<div class="container">
			<?php
				$categories = get_the_category();
				$category_id = $categories[0]->cat_ID;
				$cat_slug = $categories[0]->slug;
				$img_id = get_term_meta($category_id,'category-image-id',true);
				$img_src = wp_get_attachment_image_src( $img_id,'full')[0];
				$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				$tag_1 = get_term_by('slug','il-sole-in-classe' ,'post_tag');
				$tag_2 = get_term_by('slug','rapporti-istituzionali' ,'post_tag'); 
				$tag_3 = get_term_by('slug','storie-di-ambasciatori' ,'post_tag'); 
			?>
			
			<?php if($actual_link == get_term_link($tag_1->term_id) || $actual_link == get_term_link($tag_2->term_id) || $actual_link == get_term_link($tag_3->term_id)):?>
				<div class="container stories-header text-center" style="background:url('http://anteritalia.org/wp-content/uploads/2017/12/head1-1.jpg')">
					<h1 class="text-center text-white" >Storie di Anter</h1>
					
					<div class="tag-menu">
						<a class="<?= ($actual_link == get_term_link($tag_1->term_id)) ? 'active': ''; ?> " href="<?=get_term_link($tag_1->term_id); ?>"><?= $tag_1->name; ?></a>
						<a class="<?= ($actual_link == get_term_link($tag_2->term_id)) ? 'active': ''; ?> " href="<?=get_term_link($tag_2->term_id); ?>"><?= $tag_2->name; ?></a>
						<a class="<?= ($actual_link == get_term_link($tag_3->term_id)) ? 'active': ''; ?> " href="<?=get_term_link($tag_3->term_id); ?>"><?= $tag_3->name; ?></a>
					</div>
					
				</div>
			<?php else:?>
				<h1 class="text-center section-heading text-white bg-1" style="background:url('<?= $img_src ?>')"><?php echo single_cat_title(); ?></h1>
			<?php endif;?>
		</div>
		<?php get_template_part('loop'); ?>
	</section>
</main>
<?php get_footer(); ?>
