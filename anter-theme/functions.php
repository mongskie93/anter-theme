<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */
 if ( function_exists( 'add_theme_support' ) ) {
    load_theme_textdomain( 'anter_translation', get_template_directory() . '/languages' );
}

include plugin_dir_path(__FILE__) . '/inc/shortcode.php';
include plugin_dir_path(__FILE__) . '/inc/ajax.php';
include plugin_dir_path(__FILE__) . '/inc/theme-options.php';
include plugin_dir_path(__FILE__) . '/inc/category-image.php';
include plugin_dir_path(__FILE__) . '/inc/metaboxes.php';


function get_share_count($link){
    $ch = curl_init();
    $optArray = array(
        CURLOPT_URL => 'https://graph.facebook.com/?id='.$link,
        CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    $result = json_decode( $result );
    return $result;
}

function no_bracket_elips( $more ) {
	return '...';
}
add_filter('excerpt_more', 'no_bracket_elips');


function print_tags_on_stories($post){

    $tags = get_the_tags($post->ID);
    
    if(is_object($tags) || is_array($tags))
    foreach($tags as $tag){
        
        if (!preg_match('/feat/',$tag->name))
            echo '<a href="'.get_term_link($tag->term_id).'"><button class="tag-name">'.$tag->name.'</button></a>';
    }
    
}

add_filter('wp_nav_menu_items','add_search_box_to_menu', 10, 2);
function add_search_box_to_menu( $items, $args ) {
    if( $args->theme_location == 'main-menu' )
    
        return $items.'<li class="menu-header-search"><form action="'.get_site_url().'" id="searchform" method="get"><div id="search_form" class="input-group">
            <input type="text" class="form-control" placeholder="Cerca" name="s">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit" id="search_btn">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
            </div>
        </div></form></li>';
    return $items;
}

add_action( 'print_tags','print_tags_on_stories' ); 

function weather_widget() {
    
        register_sidebar( array(
            'name'          => 'Weather Widget',
            'id'            => 'weather_widget',
            'before_widget' => '<div class="col-xs-12 col-sm-6 col-md-4 remove-padd weather-item">',
            'after_widget'  => '</div>',
        
        ) );
    
    }

add_action( 'widgets_init', 'weather_widget' );


function wpshock_search_filter( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array('post','page') );
    }
    return $query;
}
add_filter('pre_get_posts','wpshock_search_filter');



function member_counter(){    
        $counter = '';
        $curl = curl_init(); curl_setopt_array($curl, array(
        CURLOPT_URL => "https://apigateway.nwgitalia.it/public-graphql",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_SSL_VERIFYHOST=>0,
        CURLOPT_SSL_VERIFYPEER=>0,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\r\n\t\"query\": \" query { anterMembershipCount: anterList { count } }\" \r\n}",
        CURLOPT_HTTPHEADER => array(
          "Authorization: apiKey 3gY8B6ZOveJYbK692LNolc:0knDo7vEhmbV2HbOfOdYW2",
          "Content-Type: application/json"
        ), )); $response = curl_exec($curl); $err = curl_error($curl); curl_close($curl); if ($err) {
        echo get_option( member_counter );
      } else {
        $counter = json_decode( $response)->data->anterMembershipCount[0]->count;
        $counter = number_format($counter , 0, ',', '.');
        update_option('member_counter', $counter);
        echo $counter;
      }
    
}

function youtube_thumb($id=''){
    $data = file_get_contents("https://www.googleapis.com/youtube/v3/videos?key=AIzaSyBHE2C6E_ctOl0O6R6cvv8W1Ol8N_Qy4BY&part=snippet&id=".$id);
    $json = json_decode($data);
    $yt = $json->items[0]->snippet->thumbnails;
    $url ='';
    if(isset($yt->maxres))
        return $yt->maxres->url;
    else if(isset($yt->standard))
        return $yt->standard->url;
    else if(isset($yt->high))
        return $yt->high->url;  
    else if(isset($yt->medium))
        return $yt->high->url;  
    else if(isset($yt->default))
        return $yt->default->url;  
  
}



// // add_image_size( 'feat-img', 1010, 500, array('center','center') );
add_image_size( 'thumb-img-2x', 720,480, array('center','center') );
add_image_size( 'thumb-img-1x', 360,240, array('center','center') );

// // add_image_size( 'photo-thumb', 680,680, array('center','center') );
// // add_image_size( 'slider-thumb', 360,360, array('center','center') );

