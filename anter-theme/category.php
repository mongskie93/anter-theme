<?php get_header(); ?>
	<main role="main">
		<section>
			<div class="container upper-layer">
				<?php
					$categories = get_the_category();
					$category_id = $categories[0]->cat_ID;
					$cat_slug = $categories[0]->slug;
					$img_id = get_term_meta(get_cat_ID(single_cat_title('',false)),'category-image-id',true);
					$img_src = wp_get_attachment_image_src( $img_id,'full')[0];
					
					
					
					
					
					$category_link = get_category_link(get_the_category()[0]->term_id );
				?>
				
				<?php if($cat_slug == 'anter-stories'):?>
					<div class="container stories-header text-center" style="background:url('<?= $img_src ?>')">
						<h1 class="text-center text-white" ><?php echo single_cat_title(); ?></h1>
						<?php $tag_1 = get_term_by('slug','il-sole-in-classe' ,'post_tag');
						
						 $tag_2 = get_term_by('slug','rapporti-istituzionali' ,'post_tag'); 
						$tag_3 = get_term_by('slug','storie-di-ambasciatori' ,'post_tag'); ?>
						<div class="tag-menu">
							<a href="<?=get_term_link($tag_1->term_id); ?>"><?= $tag_1->name; ?></a>
							<a href="<?=get_term_link($tag_2->term_id); ?>"><?= $tag_2->name; ?></a>
							<a href="<?=get_term_link($tag_3->term_id); ?>"><?= $tag_3->name; ?></a>
						</div>
						
					</div>
				<?php else:?>
					<h1 class="text-center section-heading text-white bg-1" style="background:url('<?= $img_src ?>')"><?php echo single_cat_title(); ?></h1>
				<?php endif;?>
			</div>
			<section id="main-1">
				<div class="container">
					<div id="post_loop" class="row row-eq-height">
					<?php 
						$counter = 1;
						$small_ad_attachment = (get_option("small_ad_attachment"))? get_option("small_ad_attachment") : '';
						$large_ad_attachment = (get_option("large_ad_attachment"))? get_option("large_ad_attachment") : '';
						$front_page_elements = get_option("theme_name_front_page_elements");
						$show_wide = ($small_ad_attachment)? 8 : 9;
					?>

					<?php if (have_posts()): while (have_posts()) : the_post(); ?>
						<?php 
							$main_cat = '';
							$main_category_link= '';
							$category = get_the_category();
							$useCatLink = true;
							// If post has a category assigned.
							if ($category){
								$category_display = '';
								$category_link = '';
								if ( class_exists('WPSEO_Primary_Term') )
								{
									// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
									$wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
									$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
									$term = get_term( $wpseo_primary_term );
									if (is_wp_error($term)) { 
										// Default to first category (not Yoast) if an error is returned
										$category_display = $category[0]->name;
										$category_link = get_category_link( $category[0]->term_id );
									} else { 
										// Yoast Primary category
										$category_display = $term->name;
										$category_link = get_category_link( $term->term_id );
									}
								} 
								else {
									// Default, display the first category in WP's list of assigned categories
									$category_display = $category[0]->name;
									$category_link = get_category_link( $category[0]->term_id );
								}
								// Display category
								if ( !empty($category_display) ){
									if ( $useCatLink == true && !empty($category_link) ){
									
										$main_category_link = $category_link;
										$main_cat = htmlspecialchars($category_display);
									
									} else {
										$main_cat = htmlspecialchars($category_display);
									}
								}
								
							}

						?>
						
						<div class="col-xs-12 col-sm-6 col-md-4 remove-padd active">    
							<div class="card-container <?= has_tag('featured') ? 'special' : '' ?>">
								<div class="card-img-container">
								<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('thumb-img',array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
									</a>
								<?php else:?>
									<a href="<?php the_permalink(); ?>">
										<img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
									</a>
								<?php endif; ?>
									<div>
										
										<a href="<?= $main_category_link ?>"><p class="card-img-text"><?= $main_cat ?></p></a>
									</div>
								</div>
								<div class="card-content">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
									<p class="card-text">
										<?php echo(get_the_excerpt()); ?>
									</p>
									<?php do_action('print_tags');?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					</div>
				</div>
			</div>

			<?php global $wp_query; ?>

			<?php if (  $wp_query->max_num_pages > 1 ): ?>
				<div class="container" id="load_more">
					<div class="center-hr">
						<span class="center-hr-element">
							<button class="misha_loadmore btn btn-v-1">MOSTRA ALTRI</button>
						</span>
					</div>
				</div>
			<?php endif; ?>
			</section>

			<?php else: ?>
				<article>
					<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
				</article>
			<?php endif; ?>
		</section>
	</main>
<?php get_footer(); ?>
