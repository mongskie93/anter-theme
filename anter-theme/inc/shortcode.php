<?php 

/**
 * gallery_ui shortcode function for [anter_gallery]
 * 
 * @return void
 */
function gallery_ui(){
    ob_start();

    $postID = get_the_ID();
    ?>
        </div>
        </div>
        </div>
            <div class="container">
                <ul id="lightSlider">
                    <?php
                        $images_id = unserialize(get_post_meta($postID)['vdw_gallery_id'][0]);
                        $loop_counter = 0; 
                        $items = "";
                        foreach($images_id as $image_id){                    
                                $items .= '<li>';
                                $items .= '<img src="'.wp_get_attachment_url($image_id).'">';
                                $items .= '<p class="image-title-a">'.get_the_title($image_id).'</p>';
                                $items .= '</li>';
                                $loop_counter++;            
                    }
                    echo $items;
                    ?>

                </ul>
            </div>
            <div class="article-view clearfix">
                <div class="share-links">
                </div>
                <div class="post-contents">
                <div class="article-content">
    <?php
    return ob_get_clean();
}
add_shortcode('anter_gallery', 'gallery_ui');


/**
 * Shortcode function for [anter_gallery_v2]
 *
 * @return void
 */
function gallery_ui_v2(){
    ob_start();
    
    $postID = get_the_ID();
    ?>
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">

        <div class="carousel-inner">

            <?php
                                        $images_id = unserialize(get_post_meta($postID)['vdw_gallery_id'][0]);
                                        $loop_counter = 0; 
                                        $items = "";
                                        foreach($images_id as $image_id){
                                            
                                            if($loop_counter == 0)
                                                $items .= '<div class="item active">';
                                            else 
                                                $items .= '<div class="item">';
                                            $items .= '<img src="'.wp_get_attachment_url($image_id).'">';
                                            $items .= '</div>';
                                            $loop_counter++;
                                            
                                            
                                            
                                        }
                                        echo $items;
                                    ?>
        </div>

<!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
        </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
    </div>


    <?php

    return ob_get_clean();

}
add_shortcode('anter_gallery_v2', 'gallery_ui_v2');


/**
 * Shortcode for function [featured_video]
 *
 * @return void
 */
function featured_video_view(){
    ob_start(); 
    $youtube_url = get_post_meta(get_the_ID(), 'featured_video', true);
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $youtube_url, $matches);
    ?>
    
    <div id="view_video" class="Video-banner">
        <input type="hidden" value=<?= $matches[0] ?>>
        <img src="<?=youtube_thumb($matches[0]);?>">
        <div class="play_container"><img src="<?=get_bloginfo('template_directory');?>/img/play_button.png" style="width: auto;margin-top: 23%;"></div>
    </div>
    
    <?php
    return ob_get_clean(); 
}
add_shortcode('featured_video', 'featured_video_view');
    

/**
 * Shortcode function for [stampa]
 *
 * @param array $atts
 * @return void
 */
function releases_shortocde($atts){
    ob_start(); 

    $show_title = ($atts['show_title'])? $atts['show_title']:'no';
    $custom_title = ($atts['custom_title'])? $atts['custom_title']:false;
    $files = get_post_meta( $atts['id'], 'pdfs', true );
    $x = get_post_meta( $atts['id'], 'display_type' , true ); 
        
    
    ?>
    <div class="row files-1">
        <?php  if($x == 'comunicati'): ?>

            <?php if($show_title == 'yes' || $show_title == 'YES'):?>

                <h3><?=  ($custom_title)? $custom_title : get_the_title($atts['id'])?></h3>

            <?php endif;?>

            <?php foreach($files as $file):    ?>

            <div class="col-xs-12 col-sm-6 margin-file">
                <div class="file">
                    <div class="file-img">
                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/file.png">
                    </div>
                    <div class="file-desc-cont">
                        <p class="file-date"><?= date_format(date_create($file['date']),"d/m/Y"); ?></p>
                        <p class="file-link"><a href="<?= wp_get_attachment_url($file['file']); ?>" target="_blank"><?= $file['name']; ?></a></p>
                        <p class="file-desc"><?= $file['desc'] ?></p>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
        <?php endif; ?>

        <?php if($x == 'rassegna'):?>
            <?php if($show_title == 'yes' || $show_title == 'YES'):?>

                <h3><?=  ($custom_title)? $custom_title : get_the_title($atts['id'])?></h3>

            <?php endif;?>

            <?php foreach($files as $file):  ?>                                

                <div class="col-xs-12 col-sm-6">
                    <div class="mag-article">
                        <p class="mag-title"><?= $file['name']; ?></p>
                        <p class="mag-desc"><?= $file['desc']?> </p>
                        <p class="mag-link"><a href="<?= $file['link']; ?>">Leggi articolo</a></p>
                    </div>
                </div>
                
            <?php endforeach; ?>

        <?php endif; ?>
    </div>

    <?php return ob_get_clean();
}
add_shortcode('stampa', 'releases_shortocde');


/**
 * shortcode function for [anter_video_gallery]
 *
 * @param [type] $atts
 * @return void
 */
function video_gallery_shortcode($atts) {
    ob_start(); 
    $counter = 1;    
    $data = get_post_meta($atts['id'], 'video_gallery_data', true);
    $title = get_post_meta($atts['id'],'video_gallery_title',true);
    $limit = get_post_meta($atts['id'], 'video_gallery_item_per_page', true);
    $youtube_url = get_post_meta($atts['id'], 'featured_video', true);

    if(isset($youtube_url)):
        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $youtube_url, $matches);

    ?>
    
        <?php if(isset($title)):?>

            <div class="gallery-year text-center">
                <h4><?= $title ?></h4>
            </div>

        <?php endif; ?>

        <?php if(isset($matches[0])): ?>

            <div  class="Video-banner">
                <iframe width="100%"  src="https://www.youtube.com/embed/<?= $matches[0] ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
            </div>

        <?php endif; ?>

    <?php endif;?>



    <div class="clearfix galc-<?=$atts['id']?>">
<?php
    $close = true;

    if(count($data) != 0 && $data != "" && $data == true)
        foreach($data as $video):
            ?>

                <div class="eco-mag-img-container square video-thumb">

                    <?php if ($video): preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $video['url'], $matches);?>
                       
                        <img class="hide-img" src="<?=youtube_thumb($matches[0]);?>">
                        <input class="youtube_url" type="hidden" name="youtube_url" value=<?= $matches[0] ?>>

                    <?php endif;?>

                        <div class="play_container">
                            <img src="<?=get_bloginfo('template_directory');?>/img/play_button.png">
                        </div>
                        <div class="square-content-overlay"></div>
                        <div class="square-content">
                            <p class="gallery-card-date"></p>
                            <p class="gallery-card-text"><?= $video['name'] ?></p>
                        </div>
                </div>
                <?php if($counter == $limit){ 
                        if($counter + 1 > sizeof($data)) {
                            $close=true;  
                        } else {
                            $close=false;  
                        }
                        break;
                    } 
                    $counter ++;       
                    $close = true; 
                ?>
                
            <?php endforeach;?>
        </div>

        <?php if($close == false):?>
            
            <div class="center-hr load-<?=$atts['id']?>">
                <span class="center-hr-element">
                    <button data-gallery-id="<?=$atts['id']?>" data-url="<?= admin_url('admin-ajax.php'); ?>" data-gallery-page=1 last-page=<?= ceil(sizeof($data)/$limit) ?> class="next-vid-page btn btn-v-1">MOSTRA ALTRI</button>
                </span>
            </div>

        <?php endif;?>
    <?php return ob_get_clean();
}
add_shortcode('anter_video_gallery', 'video_gallery_shortcode');


function photo_gallery_shortcode_no_year($atts) {
    ob_start(); 
    $close = true;
    $counter = 1;
    $title = get_post_meta($atts['id'],'photo_gallery_title',true);
    $images_id = get_post_meta($atts['id'],'vdw_gallery_id',true);
    $limit = get_post_meta($atts['id'],'photo_per_page',true);
    
    $gallery = array();
    $id_count = 0;
    foreach($images_id as $image_id) {
        $id_count++;
        $gallery[] = array(
            "id" => $id_count,
            "uploaded" => human_time_diff( get_the_date( 'U',$image_id ), current_time( 'timestamp' ) ).' '.__( 'ago' ),
            "thumb"=>  $image_id,
            "url" => wp_get_attachment_url( $image_id),
            "title"=>get_the_title($image_id)
        );
    }
    
    if(isset($title)):?>
        <div class="gallery-year text-center">
            <h4><?= $title ?></h4>
        </div>
    <?php endif; ?>
    
    <div class="container gallery-container">
            <div class="row append-<?=$atts['id']?>">
    <?php foreach($gallery as  $gallery_item) : ?>
            <div class="eco-mag-img-container square image-modal-toggle">

                <?= wp_get_attachment_image( 
                    $gallery_item['thumb'],
                    'photo-thumb', "",
                    array(
                        "class" => "hide-img",
                        "data-id" => $gallery_item['id'],
                        "gallery-id"=>$atts['id'],
                        "asrc"=>$gallery_item['url']
                         )
                    );?>

                
                <div class="square-content-overlay"></div>
            </div>
            <?php if($counter == $limit){ 
                if($counter + 1 > sizeof($gallery)) {
                    $close=true;  
                } else {
                    $close=false;  
                }
                 break;
                
                } ?>
            <?php $counter ++; ?>    
                
        
        
    <?php endforeach; ?>
        </div>
        <?php if($close == false):?>
            
            <div class="center-hr load-<?=$atts['id']?>">
                <span class="center-hr-element"><button data-gallery-id="<?=$atts['id']?>" data-url="<?= admin_url('admin-ajax.php'); ?>" data-gallery-page=1 last-page=<?= ceil(sizeof($gallery)/$limit) ?> class="next-pot-page btn btn-v-1">MOSTRA ALTRI</button></span>
            </div>
        
        
        <?php endif;?>
        </div>
                        
<?php
    return ob_get_clean();
}
add_shortcode('anter_photo_gallery', 'photo_gallery_shortcode_no_year');


function floating_menu_print(){
$floating_menu = get_post_meta( get_the_ID(), 'floating_menu_selected', true);
    if($floating_menu)
        get_template_part('floating_menu');
}

add_shortcode('anter_custom_carousel', 'anter_category_slider');
function anter_category_slider($atts,$content=""){
    $pid = $atts['id'];
    if(!$pid)
        return '';
    ?>
     <div class="">
        <div id="featured_slider" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
    <?php 
        $slides = get_post_meta($pid,'cat_list_slides',true);
        foreach($slides as $key => $slide):
        ?>
            <div class="featured-slide item <?= ($key == 0)? 'active' : ''; ?>">
                <a href="#"></a>    
                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                <img class="hide-img" src="<?= wp_get_attachment_url( $slide['image'] ); ?>">
                
                <div class="abosulte-text-slider">
                      <a href="<?= $slide['link'] ?>"><h1 class="cat-title"><?=  $slide['title']  ?></h1></a>
                      
                </div>
            </div>
        <?php endforeach; ?> 
        
        <a class="left carousel-control" href="#featured_slider" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                <a class="right carousel-control" href="#featured_slider" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
        </div>
    </div>
    </div>
    <?php
}


add_shortcode( 'anter_category_list', 'outputcategories' );


 function outputcategories($atts,$content=""){
     global $post;
     $id =  $post->ID;
     $small_ad_attachment = get_post_meta($id,'sm_ad',true);
     
?>  
    <style>
    .section-heading{
        display:none;
    }
    </style>
   
      <div class="row eco-mag-tiles-1">
            <div class="eco-mag-text">
                <p><?= $content ?></p>

            </div>
                
       <?php
       $categories = get_categories();
    ?>
    <div class="container eco-mag-container">
    <?php

       foreach($categories as $category)
       {
        $img_id = get_term_meta($category->cat_ID,'category-image-id',true);
        
        $img_src = wp_get_attachment_image_src( $img_id,'full')[0];

       ?>
          <div class="eco-mag-img-container square">
                    <img class="hide-img" src="<?= $img_src; ?>">
                    <a href="<?php echo get_category_link($category->cat_ID);?>"><h1><?= $category->name ?></h1></a>
       
            </div>
       
       <?php
       }?>
       <?php if($small_ad_attachment):?>
            <div class="eco-mag-img-container square-ad-padd">
                <a href="<?= $sm_ad_link  ?>"><img width="85%" src="<?=wp_get_attachment_url($small_ad_attachment)?>"></a>
            </div>
        <?php endif;?>
       
       </div>
       </div>
<?php
   }
  
function row_func( $atts, $content = null ) {
    return '<div class="row">' . do_shortcode($content) . '</div>';
}

function col_func( $atts, $content = null ) {
    return '<div class="col-'.$atts['size'].'">' . do_shortcode($content) . '</div>';
}
add_shortcode('row', 'row_func');
add_shortcode('col', 'col_func');


function anter_heading($atts, $content = null){
    return '<div id="'.$atts['anchor'].'" class="post-header">' . do_shortcode($content) . '</div>';
}
function anter_title( $atts, $content = null ) {
    return '<h1 class="post-title">'. do_shortcode($content).'</h1>';
}
function anter_subtitle( $atts, $content = null ) {
    return '<p class="post-short-desc">' . do_shortcode($content) . '</p>';
}

function anter_content( $atts, $content = null ) {
    return '<div class="article-content">' . do_shortcode($content) . '</div>';
}
add_shortcode('anter_content', 'anter_content');
add_shortcode('anter_title', 'anter_title');
add_shortcode('anter_heading', 'anter_heading');
add_shortcode('anter_subtitle', 'anter_subtitle');

function anter_button($atts){
    $color = ( $atts['color'] ) ? 'btn-'.$atts['size'].'-'.$atts['color'] : '';
    return '<a href="'.$atts['title'].'" class="btn-'.$atts['size'].' '.$color.'">'.$atts['title'].'</a>';
}
add_shortcode('button', 'anter_button');


add_shortcode('anter_gallery_caption', 'template_7_slider');

function template_7_slider($atts) { 
    global $post;
    $photo_ids = array();
    $db_ids = get_post_meta($post->ID, 'caption_slider',true);
    if(isset($atts['ids'])){
        $ids = str_replace(' ', '', $atts['ids']);
        $photo_ids = explode(',',$ids);
    }
    else {
        $photo_ids = get_post_meta($post->ID,'vdw_gallery_id',true);
    }
    
    
    
?>

  











  
  	<!-- thumb navigation carousel -->
    
  
  
    <!-- main slider carousel -->
    <div class="row">
        <div class="col-md-12" id="slider">
            
                <div class="col-md-12" id="carousel-bounding-box">
                    <div id="myCarousel" class="carousel slide">
                        <!-- main slider carousel items -->
                        <div class="carousel-inner">
                            <?php $countera = 0;?>
                            <?php foreach($photo_ids as $photo_id):?>
                                <?php $attachment = get_post( $photo_id ); ?>
                                <div class="item <?= ($countera == 0)? 'active':''?>" data-slide-number="<?= $countera?>7">
                                    <img src="<?= wp_get_attachment_url($photo_id); ?>" class="img-responsive">
                                    
                                    <span class="caption-slider-header">
                                            <?= $attachment->post_title; ?>
                                    </span> 
                                    <p class="caption-slider-text">

                                        <?= $attachment->post_excerpt; ?>
                                    </p>
                                </div>
                                <?php $countera++; ?>
                            <?php endforeach?>                            
                        </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

        </div>
    </div>
    <div class="scrollable1 dragscroll" id="scrollable">
    <div class=" hidden-sm hidden-xs" id="slider-thumbs">
        
            <!-- thumb navigation carousel items -->
          <ul class="list-inline">
            <?php $counter = 0;?>
            <?php foreach($photo_ids as $photo_id):?>
                <li class="inline-list-f"> <a id="carousel-selector-<?= $counter?>" class="selected">
                    <?= wp_get_attachment_image( $photo_id, 'thumbnail', "", array("class" => "img-responsive"));?>
                </a></li>
                <?php $counter++; ?>
            <?php endforeach?>
          
          </ul>
        
    </div>
    </div>
    



       

    
<?php
    // wp_register_script( 'flexslider', get_stylesheet_directory_uri() . '/js/flexslider_init.js', array('jquery'));
    // wp_enqueue_script('flexslider');
}






