<?php




// function foo_move_deck() {
//     # Get the globals:
//     global $post, $wp_meta_boxes;

//     # Output the "advanced" meta boxes:
//     do_meta_boxes( get_current_screen(), 'advanced', $post );

//     # Remove the initial "advanced" meta boxes:
//     unset($wp_meta_boxes['post']['advanced']);
// }

// add_action('edit_form_after_title', 'foo_move_deck');




function custom_excerpt_length($length)
{
    return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

function load_scrtipts_read_more()
{
    global $post;
    if($post->ID == get_option('page_on_front')){
        $args = array('post_type' => 'post','posts_per_page' => '12',);
        // The Query
        $the_query = new WP_Query($args);
    } else {
        global $wp_query;
    }
    
    

    wp_enqueue_script('jquery');
    wp_register_script('my_loadmore', get_stylesheet_directory_uri() . '/myloadmore.js', array('jquery'));
    wp_localize_script('my_loadmore', 'misha_loadmore_params', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
        'posts' => json_encode($wp_query->query_vars), // everything about your loop is here
        'current_page' => get_query_var('paged') ? get_query_var('paged') : 1,
        'max_page' => $wp_query->max_num_pages,
    ));
    wp_enqueue_script('my_loadmore');
}

add_action('wp_enqueue_scripts', 'load_scrtipts_read_more');


function admin_css_enqueue() {
    wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/assets/css/admin.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'admin_css_enqueue' );


function misha_loadmore_ajax_handler()
{
    ob_start();
    $args = json_decode(stripslashes($_POST['query']), true);
    $args['paged'] = $_POST['page'] + 1;
    $args['post_status'] = 'publish';
    $args['posts_per_page'] = 12;
    
    query_posts($args);
    ?>
    
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <?php 
            $style = '';
            if(get_post_type($post->ID) != 'page'){
                
                
                $main_cat = '';
                $main_category_link= '';
                $category = get_the_category();
                $useCatLink = true;
                // If post has a category assigned.
                if ($category){
                    $category_display = '';
                    $category_link = '';
                    if ( class_exists('WPSEO_Primary_Term') )
                    {
                        // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                        $wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
                        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                        $term = get_term( $wpseo_primary_term );
                        if (is_wp_error($term)) { 
                            // Default to first category (not Yoast) if an error is returned
                            $category_display = $category[0]->name;
                            $category_link = get_category_link( $category[0]->term_id );
                        } else { 
                            // Yoast Primary category
                            $category_display = $term->name;
                            $category_link = get_category_link( $term->term_id );
                        }
                    } 
                    else {
                        // Default, display the first category in WP's list of assigned categories
                        $category_display = $category[0]->name;
                        $category_link = get_category_link( $category[0]->term_id );
                    }
                    // Display category
                    if ( !empty($category_display) ){
                        if ( $useCatLink == true && !empty($category_link) ){
                        
                            $main_category_link = $category_link;
                            $main_cat = htmlspecialchars($category_display);
                        
                        } else {
                            $main_cat = htmlspecialchars($category_display);
                        }
                    }
                    
                }

            
            }
            else {
                $style = 'reverse_category';
                $main_cat = 'sezione';
            }
        ?>
        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd active">    
            <div class="card-container <?= has_tag('featured') ? 'special' : '' ?>">
                <div class="card-img-container">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail(array(360,240),array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
                    </a>
                <?php else:?>
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                    </a>
                <?php endif; ?>
                    <div class="<?= $style ?>">
                        <?php $main_category_link = get_category_link(get_the_category()[0]->term_id ); ?>
                        <a href="<?= $category_link ?>"><p class="card-img-text"><?= $main_cat ?></p></a>
                    </div>
                </div>
                <div class="card-content">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
                    <p class="card-text">
                        <?php echo(get_the_excerpt()); ?>
                    </p>
                    <?php do_action('print_tags');?>
                </div>
            </div>
        </div>
	<?php
        endwhile; endif;  die; 
}

add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

function limit_posts_per_archive_page()
{
    set_query_var('posts_per_page', 12);
}


function theme_menu()
{
    register_nav_menus(
        array(
            'category-menu' => __('Category Menu'),
            'main-menu' => __('Main Nav'),
            'event-menu' => __('Event Menu'),

        )
    );
}
add_action('init', 'theme_menu');

function new_submenu_class($menu)
{
    $menu = preg_replace('/ class="sub-menu"/', '/ class="dropdown-menu" /', $menu);
    return $menu;
}

add_filter('wp_nav_menu', 'new_submenu_class');

function add_link_atts($atts)
{
    $atts['class'] = "dropdown-toggle";
    return $atts;
}
add_filter('nav_menu_link_attributes', 'add_link_atts');

function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function wpb_list_child_pages()
{

    global $post;

    if (is_page() && $post->post_parent) {
        $childpages = wp_list_pages('sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0');
    } else {
        $childpages = wp_list_pages('sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0');
    }

    if ($childpages) {

        $string = '<ul>' . $childpages . '</ul>';
    }

    return $string;

}

add_shortcode('wpb_childpages', 'wpb_list_child_pages');
add_theme_support('post-thumbnails', array('post', 'page'));
add_theme_support('post-thumbnails');


/*
*
*   Photo Header for template 003
*
*/
function misha_include_myuploadscript() {
    if ( ! did_action( 'wp_enqueue_media' ) ) {
        wp_enqueue_media();
    }

    wp_enqueue_script( 'myuploadscript', get_stylesheet_directory_uri() . '/header_image.js', array('jquery'), null, false );
}

add_action( 'admin_enqueue_scripts', 'misha_include_myuploadscript' );

function misha_image_uploader_field( $name, $value = '') {
    $image = ' button">Upload image';
    $image_size = 'full';
    $display = 'none';

    if( $image_attributes = wp_get_attachment_image_src( $value, $image_size ) ) {
        $image = '"><img src="' . $image_attributes[0] . '" style="max-width:95%;display:block;" />';
        $display = 'inline-block';

    } 

    return '
    <div>
        <a href="#" class="misha_upload_image_button' . $image . '</a>
        <input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
        <a href="#" class="misha_remove_image_button" style="display:inline-block;display:' . $display . '">Remove image</a>
    </div>';
}

/*
 * Add a meta box
 */
add_action( 'admin_menu', 'misha_meta_box_add' );

function misha_meta_box_add() {
    add_meta_box('mishadiv', // meta box ID
        'More settings', // meta box title
        'misha_print_box', // callback function that prints the meta box HTML 
        'floating_menu', // post type where to add it
        'normal', // priority
        'high' ); // position
}

/*
 * Meta Box HTML
 */
function misha_print_box( $post ) {
    $meta_key = 'menu_logo';
    echo misha_image_uploader_field( $meta_key, get_post_meta($post->ID, $meta_key, true) );
    ?>
    <label for="">Select Menu Type</label>
    <?php  $menu_type = get_post_meta( $post->ID, 'floating_menu_type', true ); ?>
    <select name="floating_menu_type" id="" class="widefat">
        <option value="with_logo" <?= ($menu_type == 'with_logo')? 'selected' : ''; ?>>Default</option>
        <option value="no_logo" <?= ($menu_type == 'no_logo')? 'selected' : ''; ?>>Stampa</option>
        <option value="top_logo" <?= ($menu_type == 'top_logo')? 'selected' : ''; ?>>Anter Awards</option>
    </select>   
    <?php
}

/*
 * Save Meta Box data
 */
add_action('save_post', 'misha_save');

function misha_save( $post_id ) {
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
        return $post_id;

    $meta_key = 'menu_logo';

    update_post_meta( $post_id, $meta_key, $_POST[$meta_key] );
    if($_POST['floating_menu_type']) update_post_meta( $post_id, 'floating_menu_type', $_POST['floating_menu_type']);
    else  update_post_meta( $post_id, 'floating_menu_type', 'with_logo');
    // if you would like to attach the uploaded image to this post, uncomment the line:
    // wp_update_post( array( 'ID' => $_POST[$meta_key], 'post_parent' => $post_id ) );

    return $post_id;
}



/*
*
*  End of Photo Header for template 003
*
*/


/*
*
*  Video And Gallery CPT
*
*/
function custom_post_type()
{

    $labels = array(
        'name' => _x('Videos', 'Post Type General Name', 'twentythirteen'),
        'singular_name' => _x('Video', 'Post Type Singular Name', 'twentythirteen'),
        'menu_name' => __('Video Gallery', 'twentythirteen'),
        'parent_item_colon' => __('Parent Video Gallery', 'twentythirteen'),
        'all_items' => __('All Videos Gallery', 'twentythirteen'),
        'view_item' => __('View Video Gallery', 'twentythirteen'),
        'add_new_item' => __('Add New Video Gallery', 'twentythirteen'),
        'add_new' => __('Add New Gallery', 'twentythirteen'),
        'edit_item' => __('Edit Video Gallery', 'twentythirteen'),
        'update_item' => __('Update Video Gallery', 'twentythirteen'),
        'search_items' => __('Search Video Gallery', 'twentythirteen'),
        'not_found' => __('Not Found', 'twentythirteen'),
        'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
    );

    $args = array(
        'label' => __('videos', 'twentythirteen'),
        'description' => __('Video news and reviews', 'twentythirteen'),
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'taxonomies' => array('genres'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    register_post_type('videos', $args);

}

add_action('init', 'custom_post_type', 0);



function photos_post_type()
{

    $labels = array(
        'name' => _x('Photos', 'Post Type General Name', 'twentythirteen'),
        'singular_name' => _x('Photo', 'Post Type Singular Name', 'twentythirteen'),
        'menu_name' => __('Photo Gallery', 'twentythirteen'),
        'parent_item_colon' => __('Parent Photo  Gallery', 'twentythirteen'),
        'all_items' => __('All Photos  Gallery', 'twentythirteen'),
        'view_item' => __('View Photo  Gallery', 'twentythirteen'),
        'add_new_item' => __('Add New Photo Gallery', 'twentythirteen'),
        'add_new' => __('Add New Gallery', 'twentythirteen'),
        'edit_item' => __('Edit Photo Gallery', 'twentythirteen'),
        'update_item' => __('Update Photo Gallery', 'twentythirteen'),
        'search_items' => __('Search Photo Gallery', 'twentythirteen'),
        'not_found' => __('Not Found', 'twentythirteen'),
        'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
    );

    $args = array(
        'label' => __('Photos', 'twentythirteen'),
        'description' => __('Photo news and reviews', 'twentythirteen'),
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'taxonomies' => array('genres'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    register_post_type('Photos', $args);

}

add_action('init', 'photos_post_type', 0);

add_action('add_meta_boxes', 'photos_meta_box_add');
function photos_meta_box_add()
{
    add_meta_box('photos-meta-box-id', 'Gallery Detail', 'photos_meta_box_cb', 'Photos', 'normal', 'high');
    
}

function photos_meta_box_cb()
{
    // $post is already set, and contains an object: the WordPress post
    global $post;
    $values = get_post_custom($post->ID);
    $Photo_description = isset($values['photo_gallery_title']) ? $values['photo_gallery_title'][0] : '';
    $photo_per_page = isset($values['photo_per_page']) ? $values['photo_per_page'][0] : '';
    

    // We'll use this nonce field later on when saving.
    wp_nonce_field('my_meta_box_nonce', 'meta_box_nonce');
    ?>
    <p>Shortcode : [anter_photo_gallery id=<?= $post->ID ?>]</p>
    
    <p>
        <label for="Photo_description">Gallery Title</label>
        <input type="text"  class="widefat" name="photo_gallery_title" id="Photo_description" value="<?php echo $Photo_description; ?>">
    </p>
    <p>
        <label for="Photo_description">Photos Per Page</label>
        <input type="number"  class="widefat" name="photo_per_page" id="Photo_description" value="<?php echo $photo_per_page; ?>">
    </p>
    <?php
}

add_action('save_post', 'photos_meta_box_save');
function photos_meta_box_save($post_id)
{
    // Bail if we're doing an auto save
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // if our nonce isn't there, or we can't verify it, bail
    if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_box_nonce')) {
        return;
    }

    // if our current user can't edit this post, bail
    if (!current_user_can('edit_post')) {
        return;
    }

    // now we can actually save the data
    $allowed = array(
        'a' => array( // on allow a tags
            'href' => array(), // and those anchors can only have href attribute
        ),
    );

    // Make sure your data is set before trying to save it
    if (isset($_POST['photo_gallery_title'])) {
        update_post_meta($post_id, 'photo_gallery_title', wp_kses($_POST['photo_gallery_title'], $allowed));
    }

    if (isset($_POST['photo_per_page'])) { 
        update_post_meta($post_id, 'photo_per_page', wp_kses($_POST['photo_per_page'], $allowed));
    } else {
        update_post_meta($post_id, 'photo_per_page', '6');
    }

}




add_action('admin_init', 'hhs_add_meta_boxes', 1);
function hhs_add_meta_boxes() {
	add_meta_box( 'repeatable-fields', 'Video Gallery', 'hhs_repeatable_meta_box_display', 'videos', 'normal', 'default');
}
function hhs_repeatable_meta_box_display() {
	global $post;
    $video_gallery_data = get_post_meta($post->ID, 'video_gallery_data', true);
    $item_per_page = get_post_meta($post->ID, 'video_gallery_item_per_page', true);
    $video_gallery_title = get_post_meta($post->ID, 'video_gallery_title', true);
    $featured_video = get_post_meta($post->ID, 'featured_video', true);
	
	wp_nonce_field( 'hhs_repeatable_meta_box_nonce', 'hhs_repeatable_meta_box_nonce' );
	?>
	<script type="text/javascript">
	jQuery(document).ready(function( $ ){
		$( '#add-row' ).on('click', function() {
			var row = $( '.empty-row.screen-reader-text' ).clone(true);
			row.removeClass( 'empty-row screen-reader-text' );
			row.insertBefore( '#repeatable-fieldset-one tbody>tr:last' );
			return false;
		});
  	
		$( '.remove-row' ).on('click', function() {
			$(this).parents('tr').remove();
			return false;
		});
	});
	</script>
    <p>Copy This Shortcode [anter_video_gallery id=<?= $post->ID ?>]</p>
    <p>
        <label for="video_url">Video Gallery Title<i>(Optional)</i></label>
        <input type="text" class="widefat" name="video_gallery_title" id="video_url" value="<?php if (isset($video_gallery_title)) {echo $video_gallery_title;}?>" />
    </p>
    <p>
        <label for="featured_video">Featured Video URL<i>(Optional)</i></label>
        <input type="text" name="featured_video" id="featured_video" class="widefat" value="<?php if (isset($featured_video)) {echo $featured_video;}?>" />
    </p>
    <label>Videos Per Page</label>
    <input type="number" class="widefat" name="item_per_page" value="<?= ($item_per_page)? $item_per_page : 12 ?>" />
	<table id="repeatable-fieldset-one" width="100%">
	<thead>
		<tr>
			<th width="40%">Video Title</th>
			<th width="40%">Video Url</th>
			<th width="8%"></th>
		</tr>
	</thead>
	<tbody>
    
	<?php
	
	if ( $video_gallery_data ) :
	
	foreach ( $video_gallery_data as $field ) {
	?>
	<tr>
		<td><input type="text" class="widefat" name="name[]" value="<?php if($field['name'] != '') echo esc_attr( $field['name'] ); ?>" /></td>
	
		<td><input type="text" class="widefat" name="url[]" value="<?php if ($field['url'] != '') echo esc_attr( $field['url'] ); else echo 'http://'; ?>" /></td>
	
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	<?php
	}
	else :
	// show a blank one
	?>
	<tr>
		<td><input type="text" class="widefat" name="name[]" /></td>
	

	
		<td><input type="text" class="widefat" name="url[]" value="http://" /></td>
	
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	<?php endif; ?>
	
	<!-- empty hidden one for jQuery -->
	<tr class="empty-row screen-reader-text">
		<td><input type="text" class="widefat" name="name[]" /></td>
	

		
		<td><input type="text" class="widefat" name="url[]" value="http://" /></td>
		  
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	</tbody>
	</table>
	
	<p><a id="add-row" class="button" href="#">Add another</a></p>
	<?php
}
add_action('save_post', 'hhs_repeatable_meta_box_save');

function hhs_repeatable_meta_box_save($post_id) {
	if ( ! isset( $_POST['hhs_repeatable_meta_box_nonce'] ) ||
	! wp_verify_nonce( $_POST['hhs_repeatable_meta_box_nonce'], 'hhs_repeatable_meta_box_nonce' ) )
		return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	
	if (!current_user_can('edit_post', $post_id))
		return;
	
	$old = get_post_meta($post_id, 'video_gallery_data', true);
	$new = array();
	
	
	$names = $_POST['name'];
    $urls = $_POST['url'];
    $item_per_page = $_POST['item_per_page'];
    if(isset($_POST['item_per_page']))
        update_post_meta( $post_id, 'video_gallery_item_per_page', $_POST['item_per_page'] );
    else 
        update_post_meta( $post_id, 'video_gallery_item_per_page', 6);
    
	
	$count = count( $names );
	
	for ( $i = 0; $i < $count; $i++ ) {
		if ( $names[$i] != '' ) :
			$new[$i]['name'] = stripslashes( strip_tags( $names[$i] ) );
			
			
		
			if ( $urls[$i] == 'http://' )
				$new[$i]['url'] = '';
			else
				$new[$i]['url'] = stripslashes( $urls[$i] ); // and however you want to sanitize
		endif;
	}
    if ( !empty( $new ) && $new != $old )
		update_post_meta( $post_id, 'video_gallery_data', $new );
	elseif ( empty($new) && $old ){
        delete_post_meta( $post_id, 'video_gallery_data', $old );
    }
    
    if ($_POST['featured_video']) {
        update_post_meta($post_id, 'featured_video', $_POST['featured_video']);
    } else {
        update_post_meta($post_id, 'featured_video', '');
    }

    if ($_POST['video_gallery_title']) {
        update_post_meta($post_id, 'video_gallery_title', $_POST['video_gallery_title']);
    } else {
        update_post_meta($post_id, 'video_gallery_title', '');
    }
        
        
        
}

function custom_menu()
{

    $labels = array(
        'name' => _x('Menu', 'Post Type General Name', 'twentythirteen'),
        'singular_name' => _x('Menus', 'Post Type Singular Name', 'twentythirteen'),
        'menu_name' => __('Floating Menu', 'twentythirteen'),
        'parent_item_colon' => __('Parent Menu', 'twentythirteen'),
        'all_items' => __('All Menu', 'twentythirteen'),
        'view_item' => __('View Menu', 'twentythirteen'),
        'add_new_item' => __('Add New Menu', 'twentythirteen'),
        'add_new' => __('Add New Menu', 'twentythirteen'),
        'edit_item' => __('Edit Menu', 'twentythirteen'),
        'update_item' => __('Update Menu', 'twentythirteen'),
        'search_items' => __('Search Menu', 'twentythirteen'),
        'not_found' => __('Not Found', 'twentythirteen'),
        'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
    );

    $args = array(
        'label' => __('menu', 'twentythirteen'),
        'labels' => $labels,
        'supports' => array('title', 'custom-fields'),
        'taxonomies' => array('genres'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('floating_menu', $args);


}


add_action('init', 'custom_menu', 0);
add_action('admin_init', 'floating_menu_meta_box', 1);
function floating_menu_meta_box() {
	add_meta_box( 'repeatable-fields', 'Menu Items', 'floating_menu_box_display', 'floating_menu', 'normal', 'default');
}
function floating_menu_box_display() {
    global $post;
    $dropdown_args = array(
        'post_type'        => 'page',
        'name'             => 'pages[]',
        'sort_column'      => 'menu_order, post_title',
        'class'            => 'widefat',
        'echo'             => 1,
    );
    $video_gallery_data = get_post_meta($post->ID, 'floating_menu_items', true);
	wp_nonce_field( 'hhs_repeatable_meta_box_nonce', 'hhs_repeatable_meta_box_nonce' );
	?>
	<script type="text/javascript">
	jQuery(document).ready(function( $ ){
		$( '#add-row' ).on('click', function() {
			var row = $( '.empty-row.screen-reader-text' ).clone(true);
			row.removeClass( 'empty-row screen-reader-text' );
			row.insertBefore( '#repeatable-fieldset-one tbody>tr:last' );
			return false;
		});
  	
		$( '.remove-row' ).on('click', function() {
			$(this).parents('tr').remove();
			return false;
		});
	});
	</script>
	<table id="repeatable-fieldset-one" width="100%">
	<thead>
		<tr>
			<th width="30%">Link Title</th>
            <th width="30%">Custom Link</th>
			<th width="30%">Page</th>
			<th width="8%"></th>
		</tr>
	</thead>
	<tbody>
    
	<?php
	
	if ( $video_gallery_data ) :
	
	foreach ( $video_gallery_data as $field ) {
	?>
	<tr>
		<td><input type="text" class="widefat" name="name[]" value="<?php if($field['name'] != '') echo esc_attr( $field['name'] ); ?>" /></td>
        <td><input type="text" class="widefat" name="custom_link[]" value="<?php if($field['custom_link'] != '') echo esc_attr( $field['custom_link'] ); ?>" /></td>		
        <?php
        $new_args = array(
            'post_type'        => 'page',
            'name'             => 'pages[]',
            'sort_column'      => 'menu_order, post_title',
            'class'            => 'widefat',
            'echo'             => 1,
            'selected'    => $field['pages'],
        );
        
    ?>
		<td><?php wp_dropdown_pages( $new_args ); ?></td>
        <td><label><input type="checkbox" name="custom_menu_link[]" value="1" <?= ($field['custom_menu_link'])? 'checked':'';?>>Custom Link </label></td>
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	<?php
	}
	else :
	// show a blank one
	?>
	<tr>
		<td><input type="text" class="widefat" name="name[]" /></td>		
        <td><input type="text" class="widefat" name="custom_link[]" /></td>		
        <td><?php wp_dropdown_pages( $dropdown_args ); ?></td>
        <td><label><input type="checkbox" name="custom_menu_link[]" value="1">Custom Link</label> </td>
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	<?php endif; ?>
	
	<!-- empty hidden one for jQuery -->
	<tr class="empty-row screen-reader-text">
		<td><input type="text" class="widefat" name="name[]" /></td>
	
        <td><input type="text" class="widefat" name="custom_link[]" /></td>	
		
		<td><?php wp_dropdown_pages( $dropdown_args ); ?></td>
		<td><label><input type="checkbox" name="custom_menu_link[]" value="1">Custom Link</label> </td>
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	</tbody>
	</table>
	
	<p><a id="add-row" class="button" href="#">Add another</a></p>
	<?php
}
add_action('save_post', 'floating_menu_meta_box_save');

function floating_menu_meta_box_save($post_id) {
	if ( ! isset( $_POST['hhs_repeatable_meta_box_nonce'] ) ||
	! wp_verify_nonce( $_POST['hhs_repeatable_meta_box_nonce'], 'hhs_repeatable_meta_box_nonce' ) )
		return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	
	if (!current_user_can('edit_post', $post_id))
		return;
	
	$old = get_post_meta($post_id, 'video_gallery_data', true);
	$new = array();
	
	
	$names = $_POST['name'];
    $urls = $_POST['pages'];
    $links = $_POST['custom_link'];
    
    $disabled = $_POST['custom_menu_link'];
    
    
	$count = count( $names );
	
	for ( $i = 0; $i < $count; $i++ ) {
        if ( $names[$i] != '' ) :

            $new[$i]['custom_link'] = $links[$i];
            $new[$i]['name'] = stripslashes( strip_tags( $names[$i] ) );
            $new[$i]['custom_menu_link'] = $disabled[$i];
			
			
		
			if ( $urls[$i] == '' )
				$new[$i]['pages'] = '';
			else
				$new[$i]['pages'] = stripslashes( $urls[$i] ); // and however you want to sanitize
		endif;
	}
    if ( !empty( $new ) && $new != $old )
		update_post_meta( $post_id, 'floating_menu_items', $new );
	elseif ( empty($new) && $old )
        delete_post_meta( $post_id, 'floating_menu_items', $old );
        
        
}


add_action( 'add_meta_boxes', 'floating_menu_selector' );

/* Adds a box to the main column on the Post edit screens */
function floating_menu_selector() {
    add_meta_box( 
        'floating_menu_section',
        __( 'Select Floating Menu', '' ),
        'floating_menu_selector_ui',
        'page' ,
        'side',
        'low'
    );
}

/* Prints the box content */
function floating_menu_selector_ui( $post ) {
    ob_start();
    $selected_item = get_post_meta( $post->ID, 'floating_menu_selected', true );
    if($selected_item){
        $dropdown_args = array(
            'post_type'        => 'floating_menu',
            'name'             => 'floating_menu_selected',
            'show_option_none' => 'Disabled',
            'echo'             => 1,
            'selected'         => $selected_item,
            'class'            => 'widefat'
        );
    } else {
        $dropdown_args = array(
            'post_type'        => 'floating_menu',
            'name'             => 'floating_menu_selected',
            'show_option_none' => 'Please Select A Menu',
            'echo'             => 1,
            'class'            => 'widefat'
        );
    }
    ?>
    <label for="">Select Menu</label>
    <?php
        wp_nonce_field( 'floating_nonce', 'floating_nonce');
        wp_dropdown_pages( $dropdown_args );
    ?>
       
    <?php
    
         
}




add_action('save_post', 'floating_menu_selector_save');
function floating_menu_selector_save($post_id) {
	if ( ! isset( $_POST['floating_nonce'] ) ||
	! wp_verify_nonce( $_POST['floating_nonce'], 'floating_nonce' ) )
		return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	
    if($_POST['floating_menu_selected'])  update_post_meta( $post_id, 'floating_menu_selected', $_POST['floating_menu_selected']);
    else  delete_post_meta( $post_id, 'floating_menu_selected' );
    
        
        
}




add_action( 'add_meta_boxes', 'add_custom_box' );

    function add_custom_box( $post ) {
        add_meta_box(
            'Meta Box', // ID, should be a string.
            'Featured As Header', // Meta Box Title.
            'people_meta_box', // Your call back function, this is where your form field will go.
            'post', // The post type you want this to show up on, can be post, page, or custom post type.
            'side', // The placement of your meta box, can be normal or side.
            'core' // The priority in which this will be displayed.
        );
        add_meta_box(
            'Meta Box', // ID, should be a string.
            'Featured As Header', // Meta Box Title.
            'people_meta_box', // Your call back function, this is where your form field will go.
            'page', // The post type you want this to show up on, can be post, page, or custom post type.
            'side', // The placement of your meta box, can be normal or side.
            'core' // The priority in which this will be displayed.
        );
}

function people_meta_box($post) {
    wp_nonce_field( 'my_awesome_nonce', 'awesome_nonce' );    
    $checkboxMeta = get_post_meta( $post->ID ,'enable_image_header' , true);
    ?>

    <input type="checkbox" name="enable_image_header" id="bob" value="yes" <?= ($checkboxMeta == 'yes')? 'checked' : '' ?> />Enable Image Header<br />

<?php }

add_action( 'save_post', 'save_people_checkboxes' );
    function save_people_checkboxes( $post_id ) {
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
            return;
        if ( ( isset ( $_POST['my_awesome_nonce'] ) ) && ( ! wp_verify_nonce( $_POST['my_awesome_nonce'], plugin_basename( __FILE__ ) ) ) )
            return;
        if ( ( isset ( $_POST['post_type'] ) ) && ( 'page' == $_POST['post_type'] )  ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return;
            }    
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return;
            }
        }

        //saves bob's value
        if( isset( $_POST[ 'enable_image_header' ] ) ) {
            update_post_meta( $post_id, 'enable_image_header', 'yes' );
        } else {
            update_post_meta( $post_id, 'enable_image_header', 'no' );
        }

    
}


function Stampa_cpt()
{

    $labels = array(
        'name' => _x('Stampa Items', 'Post Type General Name', 'twentythirteen'),
        'singular_name' => _x('Stampa', 'Post Type Singular Name', 'twentythirteen'),
        'menu_name' => __('Stampa Items', 'twentythirteen'),
        'parent_item_colon' => __('Parent Stampa', 'twentythirteen'),
        'all_items' => __('All Stampa', 'twentythirteen'),
        'view_item' => __('View Stampa', 'twentythirteen'),
        'add_new_item' => __('Add New Stampa', 'twentythirteen'),
        'add_new' => __('Add New Stampa', 'twentythirteen'),
        'edit_item' => __('Edit Stampa', 'twentythirteen'),
        'update_item' => __('Update Stampa', 'twentythirteen'),
        'search_items' => __('Search Stampa', 'twentythirteen'),
        'not_found' => __('Not Found', 'twentythirteen'),
        'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
    );

    $args = array(
        'label' => __('Stampa', 'twentythirteen'),
        'labels' => $labels,
        'supports' => array('title', 'custom-fields'),
        'taxonomies' => array('genres'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('releases', $args);


}
add_action('init', 'Stampa_cpt', 0);
function Stampa_meta_box($post) {
    if ( ! did_action( 'wp_enqueue_media' ) ) {
        wp_enqueue_media();
    }
    wp_register_script( 'Stampajs', get_stylesheet_directory_uri() . '/js/releases.js', array('jquery'));
    wp_enqueue_script('Stampajs');
    $files = get_post_meta( $post->ID, 'pdfs', true );
    if(!isset($files) || $files == '' || !$files){
        $files = array();
    }
    
    
    
    
     
     ?>
     <p>Copy This Shortcode [stampa id='<?= $post->ID ?>']</p>
     <p>Link Will only work for  Rassegna Stampa Type</p>
     <input type='hidden' style='dsiplay:none;' id='img-temp' data-src="<?= get_bloginfo( 'template_directory' )?>/img/file.png">
     <h5>Select Files</h5>
        <div id="temp-val" style="display:none">
            <div class='rowx' align="center">
                <div class='cell'>
                    <label>File Selector</label>
                <?= file_selector('', 'file[file][]'); ?>
                </div>
                <div class='cell'>
                    <label>Title / Name</label> 
                    <input type="text" name="file[name][]" class="widefat" id="test">
                </div>
                
                <div class='cell'>
                    <label>Description</label>
                    <input type="text" name="file[desc][]" class="widefat" id="">
                </div>
                <div class="cell">
                    <label>Link</label>
                        <input type="text" name="file[link][]" class="widefat" id=""  value="" >
                    </div>
                <div class='cell'>
                <label>Date</label> 
                    <input type="date" name="file[date][]" class="widefat" >
                </div>
                
                <div class='cell'>
                    <a href="#" class="add-file">Remove</a>
                </div>
            </div>
        </div>
            <div class="table" id="files">
            <?php foreach($files as $file):?>
        
                <div class="rowx" align="center">
                    <div class="cell">
                        <label>File Selector</label>
                        <?= file_selector($file['file'], 'file[file][]'); ?>
                    </div>
                    <div class="cell">              
                        <label>Title / Name</label>          
                        <input type="text" name="file[name][]" class="widefat" id="" value="<?= $file['name'] ?>" >
                    </div>
                    <div class="cell">
                        <label>Description</label>          
                        <input type="text" name="file[desc][]" class="widefat" id=""  value="<?= $file['desc'] ?>" >
                    </div>
                    <div class="cell">
                        <label>Link</label>
                        <input type="text" name="file[link][]" class="widefat" id=""  value="<?= $file['link'] ?>" >
                    </div>
                    <div class="cell">
                        <label>Date</label> 
                        <input type="date" name="file[date][]" class="widefat" value="<?= $file['date'] ?>" >
                    </div>
                    
                    <div class="cell">
                        <a href="#" class="remove-file">Remove</a>
                    </div>
            
                </div>
            <?php endforeach;?>
            
                <div class="rowx" align="center">
                    <div class="cell">
                        <label>File Selector</label>
                        <?= file_selector('', 'file[file][]'); ?>
                    </div>
                    <div class="cell">
                        <label>Title / Name</label>
                        <input type="text" name="file[name][]" class="widefat" id="" value >
                    </div>
                    <div class="cell">
                        <label>Description</label>
                        <input type="text" name="file[desc][]" class="widefat" id="" >
                    </div>
                    <div class="cell">
                        <label>Link</label>
                        <input type="text" name="file[link][]" class="widefat" id=""  value="" >
                    </div>
                    <div class="cell">
                        <label>Date</label> 
                        <input type="date" name="file[date][]" class="widefat" value="">
                    </div>
                    
                    <div class="cell">
                        <a href="#" class="remove-file">Remove</a>
                    </div>
            
                </div>
                
            </div>
            <a href="#" class="add-file">Add File</a>
            <?php $x = get_post_meta( $post->ID, 'display_type' , true ); 
                
            ?>
            
            <div>
                <h5>Select Type</h5>
                <label for=""><input type="radio" name="display_type" value='rassegna' <?= ($x == 'rassegna') ? 'checked' : ''; ?>> Links Items</label>
                <br>
                <label for=""><input type="radio" name="display_type" value='comunicati' <?= ($x == 'comunicati') ? 'checked' : ''; ?>> PDF Files</label>
            </div>
    <style>
    .table{
     display: table;
    }
    .rowx {
         display: table-row;
    }
    .cell {
     display: table-cell;
     vertical-align:middle;
     box-sizing:border-box;
     padding: 0 20px;
    }
</style>
     <?php
}
function relases_meta_box_reg() {
    add_meta_box( 
        'rel_section',
        __( 'Stampa File Selection', '' ),
        'Stampa_meta_box',
        'releases' ,
        'normal',
        'high'
    );
}
add_action( 'add_meta_boxes', 'relases_meta_box_reg' );

function file_selector($value = '',$name) {
    $image = ' button">Upload File';
    $image_size = 'full'; // it would be better to use thumbnail size here (150x150 or so)
    $display = 'none'; // display state ot the "Remove image" button

    if( $image_attributes = wp_get_attachment_url( $value ) ) {
        $image = '"><img src="'.get_bloginfo( 'template_directory' ).'/img/file.png" style="margin:0 auto;max-width:400px;display:block;" />';
        $display = 'block';
        
    } 

    return '
    <div style="width:100px; text-align:center;">
        <a href="#" class="pdf_release_btn' . $image . '</a>
        <input type="hidden" name="'.$name.'" id="small_ad" value="' . $value . '" />
        <a href="#" class="remove_files_s" style="display:block;display:' . $display . '">Remove File</a>
    </div>';
}

function save_release_file($post)
{
    global $post;
    

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return false;
    }
    
    $details = array();
    for($x = 0 ; $x < sizeof($_POST['file']['name']); $x ++){
        $temp = array();
        if($_POST['file']['name'][$x] != ''){
            $temp['file'] = $_POST['file']['file'][$x];
            $temp['link'] = $_POST['file']['link'][$x];
            $temp['name'] = $_POST['file']['name'][$x];
            $temp['desc'] = $_POST['file']['desc'][$x];
            $temp['date'] = $_POST['file']['date'][$x];
            $details[] = $temp;
        }
    }

    if(isset($_POST['display_type'])) 
        update_post_meta( $post->ID, 'display_type' ,$_POST['display_type'] );
    else
        update_post_meta( $post->ID, 'display_type' , 'rassegna' );
    
    
    update_post_meta( $post->ID, 'pdfs' , $details );
    

    
}
add_action('save_post', 'save_release_file');



function homepage_sliders($post){
    
    $p_wide = get_post_meta($post->ID,'p_wide',true);
    $p_wide_cap = get_post_meta($post->ID,'p_wide_cap',true);
    $p_sm_1 = get_post_meta($post->ID,'p_sm_1',true);
    $p_sm_1_cap = get_post_meta($post->ID,'p_sm_1_cap',true);
    $p_sm_2 = get_post_meta($post->ID,'p_sm_2',true);
    $p_sm_2_cap = get_post_meta($post->ID,'p_sm_2_cap',true);
    $p_wide_cap_link = get_post_meta($post->ID,'p_wide_cap_link',true);
    $p_sm_1_cap_link = get_post_meta($post->ID,'p_sm_1_cap_link',true);
    $p_sm_2_cap_link = get_post_meta($post->ID,'p_sm_2_cap_link',true);

    $sm_ad = get_post_meta($post->ID,'sm_ad',true);
    $lg_ad = get_post_meta($post->ID,'lg_ad',true);
    $sm_ad_link = get_post_meta($post->ID,'sm_ad_link',true);
    $lg_ad_link = get_post_meta($post->ID,'lg_ad_link',true);

    ?> 
        
                
                
                <?php if(get_post_meta( $post->ID, "set_page_as", true ) != 'cat_list'):   ?>  
                    <label for="">Wide Photo (690 x 255)px</label>
                    <?php echo image_selector($p_wide,'p_wide');?>
                    <label for="">Wide Photo Caption </label>
                    <input class="widefat" type="text" name="p_wide_cap" value="<?= ($p_wide_cap)? $p_wide_cap : ''; ?>">
                    <label for="">Wide Photo Link</label>
                    <input class="widefat" type="text" name="p_wide_cap_link" value="<?= ($p_wide_cap_link)? $p_wide_cap_link : ''; ?>">
                    <label for="">Small Photo 1(345 x 255)px</label>
                    <?php echo image_selector($p_sm_1,'p_sm_1');?>
                    <label for="">Small Photo 1 Caption</label>
                    <input class="widefat" type="text" name="p_sm_1_cap" value="<?= ($p_sm_1_cap)? $p_sm_1_cap : ''; ?>">
                    <label for="">Small Photo 1 Link</label>
                    <input class="widefat" type="text" name="p_sm_1_cap_link" value="<?= ($p_sm_1_cap_link)? $p_sm_1_cap_link : ''; ?>">
                    
                    <label for="">Small Photo 2 (345 x 255)px</label>
                    <?php echo image_selector($p_sm_2,'p_sm_2');?>
                    <label for="">Small Photo 2 Caption</label>
                    <input class="widefat" type="text" name="p_sm_2_cap" value="<?= ($p_sm_2_cap)? $p_sm_2_cap : ''; ?>">
                    <label for="">Small Photo 2 Link</label>
                    <input class="widefat" type="text" name="p_sm_2_cap_link" value="<?= ($p_sm_2_cap_link)? $p_sm_2_cap_link : ''; ?>">

                    <br>
                    <br>
                <?php endif;?>
                <!-- <label for="">Small Ad Banner Image Size (300 x 250)px</label>
                <?php echo image_selector($sm_ad,'sm_ad');?> -->
                <!-- <label for="">Small Ad Banner Link</label>
                <input class="widefat" type="text" name="sm_ad_link" value="<?= ($sm_ad_link)? $sm_ad_link : ''; ?>"> -->
                <label for="">Large Ad Banner (950 x 250)px</label>
                <?php echo image_selector($lg_ad,'lg_ad');?>
                <label for="">Large Ad Banner Link</label>
                <input class="widefat" type="text" name="lg_ad_link" value="<?= ($lg_ad_link)? $lg_ad_link : ''; ?>">
    
    <?php
}

function homepage_slider_func($postType)
{
    global $post;
    if(!empty($post))
    {
    $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);
        if($post->ID == get_option( 'page_on_front' ) || get_post_meta( $post->ID, "set_page_as", true ) == 'cat_list')
        {
            $types = array('page');
            if (in_array($postType, $types)) {
                add_meta_box('homepage_sliders', 'Home Page Photos / Banners', 'homepage_sliders', $postType, 'normal', 'high'); ## Adds a meta box to post type
            }
        }
    }   
}
add_action('add_meta_boxes', 'homepage_slider_func');

function homepage_slider_save( $post ) {
    global $post;
    

    if(isset($_POST['p_wide']) ) update_post_meta( $post->ID, 'p_wide', $_POST['p_wide']);
    else delete_post_meta( $post->ID, 'p_wide' );

    if(isset($_POST['p_sm_1']) ) update_post_meta( $post->ID, 'p_sm_1', $_POST['p_sm_1']);
    else delete_post_meta( $post->ID, 'p_sm_1' );

    if(isset($_POST['p_sm_2'])) update_post_meta( $post->ID, 'p_sm_2', $_POST['p_sm_2']);
    else delete_post_meta( $post->ID, 'p_sm_2' );
    
    if(isset($_POST['p_wide_cap']) ) update_post_meta( $post->ID, 'p_wide_cap', $_POST['p_wide_cap']);
    else delete_post_meta( $post->ID, 'p_wide_cap' );

    if(isset($_POST['p_sm_1_cap']) ) update_post_meta( $post->ID, 'p_sm_1_cap', $_POST['p_sm_1_cap']);
    else delete_post_meta( $post->ID, 'p_sm_1_cap' );

    
    if(isset($_POST['p_sm_2_cap'])) update_post_meta( $post->ID, 'p_sm_2_cap', $_POST['p_sm_2_cap']);
    else delete_post_meta( $post->ID, 'p_sm_2_cap' );


    if(isset($_POST['p_wide_cap_link'])) update_post_meta( $post->ID, 'p_wide_cap_link', $_POST['p_wide_cap_link']);
    else delete_post_meta( $post->ID, 'p_wide_cap_link' );

    if(isset($_POST['p_sm_1_cap_link'])) update_post_meta( $post->ID, 'p_sm_1_cap_link', $_POST['p_sm_1_cap_link']);
    else delete_post_meta( $post->ID, 'p_sm_1_cap_link' );

    if(isset($_POST['p_sm_2_cap_link'])) update_post_meta( $post->ID, 'p_sm_2_cap_link', $_POST['p_sm_2_cap_link']);
    else delete_post_meta( $post->ID, 'p_sm_2_cap_link' );



    if(isset($_POST['sm_ad'])) update_post_meta( $post->ID, 'sm_ad', $_POST['sm_ad']);
    else delete_post_meta( $post->ID, 'sm_ad' );

    if(isset($_POST['lg_ad'])) update_post_meta( $post->ID, 'lg_ad', $_POST['lg_ad']);
    else delete_post_meta( $post->ID, 'lg_ad' );

    if(isset($_POST['sm_ad_link'])) update_post_meta( $post->ID, 'sm_ad_link', $_POST['sm_ad_link']);
    else delete_post_meta( $post->ID, 'sm_ad_link' );

    if(isset($_POST['lg_ad_link'])) update_post_meta( $post->ID, 'lg_ad_link', $_POST['lg_ad_link']);
    else delete_post_meta( $post->ID, 'lg_ad_link' );





 
    
}

/*
 * Save Meta Box data
 */
add_action('save_post', 'homepage_slider_save');


function cateory_List_meta_box_reg() {
    
    add_meta_box( 
        'rel_section',
        __( 'Custom Carousel', '' ),
        'category_list_sliders',
        'carousel' ,
        'normal',
        'high'
    );
    
}
add_action( 'add_meta_boxes', 'cateory_List_meta_box_reg' );



function category_list_sliders($post) {
    
    if ( ! did_action( 'wp_enqueue_media' ) ) {
        wp_enqueue_media();
    }
    wp_register_script( 'Stampajs', get_stylesheet_directory_uri() . '/js/categorylist.js', array('jquery'));
    wp_enqueue_script('Stampajs');
    
    $slides = get_post_meta( $post->ID, 'cat_list_slides', true );
    
    if(!isset($slides) || $slides == '' || !$slides){
        $slides = array();
    }
     ?>
     
    <input type="hidden" name="pid" value="<?=$post->ID?>"> 
    <h2>For Custom Carousel Slider Only</h2>
    <p>Shortcode : [anter_custom_carousel id=<?=$post->ID?>]</p>
     <h5>Slides for Category List</h5>
        <div id="temp-val" style="display:none">
            <div class='rowx' align="center">
                <div class='cell'>
                <label>Image Selector</label>
                    <?= slide_selector('', 'slide[image][]'); ?>
                </div>
                <div class='cell'>
                    <label>Title / Name</label> 
                    <input type="text" name="slide[title][]" class="widefat" id="test">
                </div>
                
                <div class='cell'>
                    <label>Link</label>
                    <input type="text" name="slide[link][]" class="widefat" id="">
                </div>
                
                <div class='cell'>
                    <a href="#" class="add-file">Remove</a>
                </div>
            </div>
        </div>
            <div class="table" id="files">
            <?php foreach($slides as $slide):?>
        
                <div class="rowx" align="center">
                    <div class="cell">
                        <label>Image Selector</label>
                        <?= slide_selector($slide['image'], 'slide[image][]'); ?>
                    </div>
                    <div class="cell">              
                        <label>Title / Name</label>          
                        <input type="text" name="slide[title][]" class="widefat" id="" value="<?= $slide['title'] ?>" >
                    </div>

                    <div class="cell">
                        <label>Link</label>
                        <input type="text" name="slide[link][]" class="widefat" id=""  value="<?= $slide['link'] ?>" >
                    </div>
                    
                    <div class="cell">
                        <a href="#" class="remove-file">Remove</a>
                    </div>
            
                </div>
            <?php endforeach;?>
            <div class='rowx' align="center">
                <div class='cell'>
                <label>Image Selector</label>
                    <?= slide_selector('', 'slide[image][]'); ?>
                </div>
                <div class='cell'>
                    <label>Title / Name</label> 
                    <input type="text" name="slide[title][]" class="widefat" id="test">
                </div>
                
                <div class="cell">
                        <label>Link</label>
                        <input type="text" name="slide[link][]" class="widefat" id=""  value="<?= $slide['link'] ?>" >
                    </div>
                <div class='cell'>
                    <a href="#" class="add-file">Remove</a>
                </div>
            </div>
                
            </div>
            <a href="#" class="add-file">Add File</a>
            
    <style>
        .table{
         display: table;
        }
        .rowx {
             display: table-row;
        }
        .cell {
         display: table-cell;
         vertical-align:middle;
         box-sizing:border-box;
         padding: 0 20px;
        }
        .misha_upload_image_button img{
            width:100px;
        }
    </style>
     <?php
}



function slide_selector($value = '',$name) {
    $image = ' button">Upload image';
    $image_size = 'full';
    $display = 'none';

    if( $image_attributes = wp_get_attachment_image_src( $value, $image_size ) ) {
        $image = '"><img src="' . $image_attributes[0] . '" style="max-width:95%;display:block;" />';
        $display = 'inline-block';

    } 

    return '
    <div>
        <a href="#" class="misha_upload_image_button' . $image . '</a>
        <input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
        <a href="#" class="misha_remove_image_button" style="display:inline-block;display:' . $display . '">Remove image</a>
    </div>';
}


function save_slider_items($post)
{
    global $post;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return false;
    }
    $details = array();
    for($x = 0 ; $x < sizeof($_POST['slide']['title']); $x ++){
        $temp = array();
        if($_POST['slide']['title'][$x] != ''){
            $temp['image'] = $_POST['slide']['image'][$x];
            $temp['title'] = $_POST['slide']['title'][$x];
            $temp['link'] = $_POST['slide']['link'][$x];
            
            $details[] = $temp;
        }
    }
    
    update_post_meta( $_POST["pid"], 'cat_list_slides' , $details );    
}
add_action('save_post', 'save_slider_items');

function carousel_post_type()
{
    $labels = array(
        'name' => _x('Carousel', 'Post Type General Name', 'twentythirteen'),
        'singular_name' => _x('Carousel', 'Post Type Singular Name', 'twentythirteen'),
        'menu_name' => __('Custom Carousel', 'twentythirteen'),
        'parent_item_colon' => __('Parent Photo  Gallery', 'twentythirteen'),
        'all_items' => __('All Carousel', 'twentythirteen'),
        'view_item' => __('View Carousel', 'twentythirteen'),
        'add_new_item' => __('Add New Carousel', 'twentythirteen'),
        'add_new' => __('Add New Carousel', 'twentythirteen'),
        'edit_item' => __('Edit Photo Carousel', 'twentythirteen'),
        'update_item' => __('Update Carousel', 'twentythirteen'),
        'search_items' => __('Search Carousel', 'twentythirteen'),
        'not_found' => __('Not Found', 'twentythirteen'),
        'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
    );

    $args = array(
        'label' => __('Carousel', 'twentythirteen'),
        'description' => __('Carousel news and reviews', 'twentythirteen'),
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'taxonomies' => array('genres'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    register_post_type('carousel', $args);

}

add_action('init', 'carousel_post_type', 0);