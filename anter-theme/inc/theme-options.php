<?php
// THEME OPTIONS

function register_category_menu_sub_menu_page() {
    add_submenu_page( 'anteroptions', 'Category List Options', 'Category List', 'add_users', 'category_list', 'category_list_page' );
}
function register_releases_menu_sub_menu_page() {
    add_submenu_page( 'anteroptions', 'Releases Option', 'Releases Option', 'add_users', 'releases_option', 'stampa_options' );
}
function register_custom_menu_page() {
    add_menu_page('Anter Theme Settings', 'Theme Settings', 'add_users', 'anteroptions', '_custom_menu_page', null, 30); 
}
add_action('admin_menu', 'register_custom_menu_page');
//add_action('admin_menu', 'register_releases_menu_sub_menu_page');
//add_action('admin_menu', 'register_category_menu_sub_menu_page');



function stampa_options(){
    wp_register_script( 'releases', get_stylesheet_directory_uri() . '/js/releases-options.js', array('jquery'));
    wp_enqueue_script('releases');
    $releases_contacts = (get_option("releases_contacts"))? get_option("releases_contacts") : '';
    
    ?>
        <form method="post">
            <h2>Releases Options&nbsp;<span><input type="submit" value="Save settings" class="button"/></span></h2>
        <?php 
        echo '<pre>';
      
        //print_r($_POST['contact_info']);
        // $contact = array();
        // foreach($_POST['contact_info'] as $contact_details){
        //     $temp = array();
        //     $temp['name'] = $
        // }
        echo "</pre>";
        if (isset($_POST["update_settings"])) {
            
            $details = array();
            for($x = 0 ; $x < sizeof($_POST['contact_info']['name']); $x ++){
                $temp = array();
                if($_POST['contact_info']['name'][$x] != ''){
                    $temp['name'] = $_POST['contact_info']['name'][$x];
                    $temp['detail'] = $_POST['contact_info']['detail'][$x];
                    $temp['number'] = $_POST['contact_info']['number'][$x];
                    $temp['email'] = $_POST['contact_info']['email'][$x];
                    $details[] = $temp;
                }
            }
            $releases_contacts =$details;
            
            update_option("releases_contacts", $details);
    
        ?>
            <div id="message" class="updated">Settings saved</div>
        <?php
        }

        ?>
        <p>Contact Details</p>
        <div class="t-06-contact-container">
            <?php foreach($releases_contacts as $contact):?>
            <div class="t-06-contact">
                <input type="text" name="contact_info[name][]" placeholder="Full Name" value=<?= $contact['name']?>>
                <input type="text" name="contact_info[detail][]" placeholder="Details" value=<?= $contact['detail']?>>
                <input type="text" name="contact_info[number][]" placeholder="Contact Number" value=<?= $contact['number']?>>
                <input type="text" name="contact_info[email][]" placeholder="Email" value=<?=$contact['email']?>>
                <a class="remove-row">Remove Details</a>
            </div>

            <?php endforeach; ?>
            <div class="t-06-contact">
                <input type="text" name="contact_info[name][]" placeholder="Full Name">
                <input type="text" name="contact_info[detail][]" placeholder="Details">
                <input type="text" name="contact_info[number][]" placeholder="Contact Number">
                <input type="text" name="contact_info[email][]" placeholder="Email">
                <a class="remove-row">Remove Details</a>
            </div>
        </div>
        
        <br>
        <a href="#" id="add-f">Add Details</a>
        <input type="hidden" name="update_settings" value="Y">
        </form>
        <div id="defaulter" style="display:none">
            <div class="t-06-contact">
                <input type="text" name="contact_info[name][]" placeholder="Full Name">
                <input type="text" name="contact_info[detail][]" placeholder="Details">
                <input type="text" name="contact_info[number][]" placeholder="Contact Number">
                <input type="text" name="contact_info[email][]" placeholder="Email">
                <a class="remove-row">Remove Details</a>
            </div>
        </div>
    <?php
}

function category_list_page(){

    $category_list_text = (get_option("category_list_text"))? get_option("category_list_text") : '';
    if (isset($_POST["update_settings"])) {
        update_option("category_list_text", $_POST['category_list_text']);
        $category_list_text = $_POST['category_list_text'];
    
    

    ?>
        <div id="message" class="updated">Settings saved</div>
    <?php
    }

    ?>
    <div class="wrap">
    <form method="POST" action="">  
        <div class="wrap" style="width:40%; display:inline-block;">
        </div>

        
        <h2>Anter Theme Category List Options&nbsp;<span><input type="submit" value="Save settings" class="button"/></span></h2>
        <input type="hidden" name="update_settings" value="Y" />
        <p>Category List Shortcode [anter_category_list]<br>Add Category Background Images ounder Pages -> Category</p>
        <label for="">Category List Text</label>
        <textarea class="widefat" name="category_list_text" id="" cols="30" rows="10" placeholder="Category List Text"><?= $category_list_text; ?></textarea>
    </div>

<?php
}

function _custom_menu_page(){
    if(isset($_POST["popupsub"])){
        update_option( "homepagepopup_url", $_POST["homepagepopup_url"] );
        if( isset($_POST["homepagepopup_url"])){
            if( $_POST["control"] == "disable")
                update_option( "control", $_POST["control"] );
            else 
                update_option( "control", "enable" );
        }   
    }
   ?>
   
    <form action="" method="post">
    <h1>Homepage Video Pop Up</h1>
    <input type="text" name="homepagepopup_url" id="homepagepopup" value="<?= get_option( "homepagepopup_url" ) ?>">
    <br>
    <label><input type="checkbox" value="disable" name="control" <?= (get_option( "control"  ) == "disable") ? "checked" : ""  ?>>Disable</label>
    <br>
    <input type="submit" value="submit" name="popupsub">
    </form>

   <?php
   
}


function get_popup_video(){
    $matches = array();
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", get_option("homepagepopup_url"), $matches);
    echo $matches[0];
    die();
}

add_action( 'wp_ajax_get_popup_video', 'get_popup_video' );
add_action( 'wp_ajax_nopriv_get_popup_video', 'get_popup_video' );

function image_selector($value = '',$name) {
    $image = ' button">Upload image';
    $image_size = 'full'; // it would be better to use thumbnail size here (150x150 or so)
    $display = 'none'; // display state ot the "Remove image" button

    if( $image_attributes = wp_get_attachment_image_src( $value ) ) {

        // $image_attributes[0] - image URL
        // $image_attributes[1] - image width
        // $image_attributes[2] - image height

        $image = '"><img src="' . $image_attributes[0] . '" style="max-width:400px;display:block;" />';
        $display = 'block';

    } 

    return '
    <div>
        <a href="#" class="misha_upload_image_button' . $image . '</a>
        <input type="hidden" name="'.$name.'" id="small_ad" value="' . $value . '" />
        <a href="#" class="misha_remove_image_button" style="display:block;display:' . $display . '">Remove image</a>
    </div>';
}



?>