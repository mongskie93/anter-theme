<?php
/**
 * Template Name: Default Article Template
 */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="main-content" class="main-content regular-width">


<?php $second_featured_img = get_post_meta(get_the_ID(), 'second_featured_img', true); ?>

<?php if ( has_post_thumbnail() && get_post_meta( $post->ID ,'enable_image_header' , true) == 'yes') : // Check if Thumbnail exists ?>
	

<section id="blur-bg" class="text-center">
        <div class="article-hero-slider"></div>
        <div class="article-hero-img-cont">
        	<div class="article-hero-img text-center">
				<?= the_post_thumbnail( 'full',array('id'=>'srouce-image-hero','class'=>'feat-img') ); ?>
        	</div>
    	</div>
</section>
<?php endif; ?>
<section id="article-main-container">

<div class="container">
			<?php 
                $ch = curl_init();
                $optArray = array(
                    CURLOPT_URL => 'https://graph.facebook.com/?id='.get_permalink(),
                    CURLOPT_RETURNTRANSFER => true
                );
                curl_setopt_array($ch, $optArray);
                $result = curl_exec($ch);
                $result = json_decode( $result );
            ?>
			<?php 
				$floating_menu = get_post_meta( get_the_ID(), 'floating_menu_selected', true);

				
				if($floating_menu){
					get_template_part('floating_menu');
					
					
				}	

					
					
			?>
	<div id="main-article" class="article-view clearfix">
		
		<div class="share-links " style="margin-top: 300px;<?php if(get_post_meta( get_the_ID(), "disable_social_link", true )[0] == 'yes') echo 'display:none;'; ?>" >
		<?php if(get_post_meta( get_the_ID(), "disable_social_link", true )[0] != 'yes'):?>				
			<h1 class="share-count-bold"><?= $result->share->share_count ?></h1>
			<p class="share-count-label">condivisioni</p>
			<div class="social-media-share-button">
				
			<div class="share-buttons"><a href="<?= 'https://facebook.com/sharer/sharer.php?u='.urlencode(get_permalink()) ?>" 
			onclick="window.open('<?= 'https://facebook.com/sharer/sharer.php?u='.urlencode(get_permalink()) ?>','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"
			><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/fb.png"></a></div>
		   
			<div class="share-buttons"><a href="https://twitter.com/share?url=<?=urlencode(get_permalink()) ?>"
			onclick="window.open('https://twitter.com/share?url=<?=urlencode(get_permalink()) ?>','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
			<img src="<?php echo get_bloginfo( 'template_directory' );?>/img/tw.png"></a></div>
			<div class="share-buttons"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=urlencode(get_permalink()) ?>"><img width="100%" src="<?php echo get_bloginfo( 'template_directory' );?>/img/in-1.png"></a></div>
			</div>
		<?php endif; ?>
		</div>
		
		<div class="post-contents <?php if(get_post_meta( get_the_ID(), "disable_social_link", true )[0] == 'yes') echo 'default_wide'; ?>	">
			<div class="post-header">
			<?php if(get_post_meta( get_the_ID(), "disable_page_title", true )[0] != 'yes'):?>	
				<h1 class="post-title"><?php the_title(); ?></h1>
			<?php endif; ?>
				<?php $subtitle = get_post_meta(get_the_ID(), 'sub_title', true);?>
				<p class="post-short-desc"><?php if(isset($subtitle)) echo $subtitle; ?></p>
			</div>
			<div class="post-main-image">
					
			</div>
			<div class="article-content">
				<p><?php echo the_content();?></p>
			</div>
		</div>
	</div>
</div>
</section>
<?php if(get_post_meta( $post->ID, "enable_comment", true )[0] == 'enabled'):?>
<!-- <div class="container">
    <div class="center-hr chrv2">
       	<span class="center-hr-element">
	        <div class="commenti"><p>commenti</p></div>
       	</span>
    </div>
</div>


<section>            
	<div class="container text-center">
		<div class="fb-comments" data-href="<?= get_permalink(); ?>" data-numposts="5"></div>
	</div>
</section> -->
<?php endif; ?>


<?php endwhile; ?>
<?php endif; ?>

<?php if(get_post_meta( $post->ID, 'enable_related_post', true )[0] == 1):?>
<div class="container">
	<div class="center-hr chrv2">
		<span class="center-hr-element">
			<div class="commenti"><p>CONTINUA CON</p></div>
		</span>
	</div>
</div>
<div class="container">
<div class="row  row-eq-height class rel-article">
<?php
$related_post = array(
    get_post_meta( $post->ID, 'rp_1', true ),
    get_post_meta( $post->ID, 'rp_2', true ),
    get_post_meta( $post->ID, 'rp_3', true )
);

$rem = 3;

foreach($related_post as $key=>$rel)
    if(!$rel)
        unset($related_post[$key]);
    else
        $rem -= 1;

$orig_post = $post; 
global $post;



if(count($related_post) > 0)
	$args = array(
		'post__not_in' => array($post->ID),
		'post__in'      => $related_post,
		'post_type' => array('post','page')
	);

$my_query = new wp_query( $args );  
$rem = $rem - $news_2->my_query;
if($rem != 3)
while( $my_query->have_posts() ) {
    $my_query->the_post();
    
    
?>

        
        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd">
            <div class="card-container">
                <div class="card-img-container">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumb-img',array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
                    </a>
                <?php else:?>
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                    </a>
                <?php endif; ?>
                    
                    <div>
                        <?php 
                        
                           $category_link = get_category_link(get_the_category()[0]->term_id );
                           ?>
                        <a href="<?= $category_link ?>"><p class="card-img-text"><?= (isset(get_the_category()[0]->cat_name)) ? get_the_category()[0]->cat_name : 'pagina'; ?></p></a>

                    </div>
                </div>
                <div class="card-content">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
                    <p class="card-text">
                        <?php echo(get_the_excerpt()); ?>
                    </p>
                </div>
            </div>
        </div>

    
    

<?php }
$post = $orig_post;
wp_reset_query();


$orig_post = $post;
global $post;
$related_post[] = $post->ID;
$tags = wp_get_post_tags($post->ID);

$args=array(
    'post__not_in' => array($related_post),
    'posts_per_page'=>$rem, // Number of related posts to display.
    'ignore_sticky_posts'=>1
);
if(get_the_category()[0]->count > 1 && get_the_category()[0]->cat_name != 'Uncategorized')
    $args['category_name'] = get_the_category()[0]->name;

$my_query = new wp_query( $args );  
if($rem != 0)
while( $my_query->have_posts() ) {
    $my_query->the_post();
  
?>

        
        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd">
            <div class="card-container">
                <div class="card-img-container">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumb-img',array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
                    </a>
                <?php else:?>
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                    </a>
                <?php endif; ?>
                    
                    <div>
                        <?php 
                        
                            $category_link = get_category_link(get_the_category()[0]->term_id );
                            ?>
                        <a href="<?= $category_link ?>"><p class="card-img-text"><?= get_the_category()[0]->cat_name; ?></p></a>

                    </div>
                </div>
                <div class="card-content">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
                    <p class="card-text">
                        <?php echo(get_the_excerpt()); ?>
                    </p>
                </div>
            </div>
        </div>

    
    

<?php }

$post = $orig_post;
wp_reset_query();
?>

</div>
</div>

<?php endif; ?>
<?php
get_footer();


?>

