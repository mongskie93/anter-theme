<?php
/**
 * Template Name: Wide Template
 * Template Post Type: post, page, product
 */
get_header();
if ( is_home() || is_front_page() ) {
    get_template_part("homepage");
} else {
    get_template_part("default_wide");
}
?>

<?php if(get_post_meta( $post->ID, 'enable_related_post', true )[0] == 1):?>
<div class="container">
	<div class="center-hr chrv2">
		<span class="center-hr-element">
			<div class="commenti"><p>CONTINUA CON</p></div>
		</span>
	</div>
</div>
<div class="container">
<div class="row  row-eq-height class rel-article">
<?php
$related_post = array(
    get_post_meta( $post->ID, 'rp_1', true ),
    get_post_meta( $post->ID, 'rp_2', true ),
    get_post_meta( $post->ID, 'rp_3', true )
);

$rem = 3;

foreach($related_post as $key=>$rel)
    if(!$rel)
        unset($related_post[$key]);
    else	
        $rem -= 1;

$orig_post = $post; 
global $post;



if(count($related_post) > 0):
	$args = array(
		'post__not_in' => array($post->ID),
		'post__in'      => $related_post,
		'post_type' => array('post','page')
	);

$my_query = new wp_query( $args );  
$rem = $rem - $news_2->my_query;
if($rem != 3)
while( $my_query->have_posts() ) {
    $my_query->the_post();
    
    
?>

        
        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd">
            <div class="card-container">
                <div class="card-img-container">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumb-img',array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
                    </a>
                <?php else:?>
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                    </a>
                <?php endif; ?>
                    
                    <div>
                        <?php 
                        
                           $category_link = get_category_link(get_the_category()[0]->term_id );
                           ?>
                        <a href="<?= $category_link ?>"><p class="card-img-text"><?= (isset(get_the_category()[0]->cat_name)) ? get_the_category()[0]->cat_name : 'pagina'; ?></p></a>

                    </div>
                </div>
                <div class="card-content">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
                    <p class="card-text">
                        <?php echo(get_the_excerpt()); ?>
                    </p>
                </div>
            </div>
        </div>

    
    

<?php }
$post = $orig_post;
wp_reset_query();
endif;

$orig_post = $post;
global $post;
$related_post[] = $post->ID;
$tags = wp_get_post_tags($post->ID);

$args=array(
    'post__not_in' => array($related_post),
    'posts_per_page'=>$rem, // Number of related posts to display.
    'ignore_sticky_posts'=>1
);
if(get_the_category()[0]->count > 1 && get_the_category()[0]->cat_name != 'Uncategorized')
    $args['category_name'] = get_the_category()[0]->name;

$my_query = new wp_query( $args );  

if($rem != 0)
while( $my_query->have_posts() ) {
    $my_query->the_post();
  
?>

        
        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd">
            <div class="card-container">
                <div class="card-img-container">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumb-img',array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
                    </a>
                <?php else:?>
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                    </a>
                <?php endif; ?>
                    
                    <div>
                        <?php 
                        
                            $category_link = get_category_link(get_the_category()[0]->term_id );
                            ?>
                        <a href="<?= $category_link ?>"><p class="card-img-text"><?= get_the_category()[0]->cat_name; ?></p></a>

                    </div>
                </div>
                <div class="card-content">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
                    <p class="card-text">
                        <?php echo(get_the_excerpt()); ?>
                    </p>
                </div>
            </div>
        </div>

    
    

<?php }

$post = $orig_post;
wp_reset_query();
?>

</div>
</div>

<?php endif; ?>

<?php get_footer(); ?>
