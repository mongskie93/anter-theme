<section id="main-1" class="wide-template">
<?php 
$result = get_share_count(get_permalink());

if (have_posts()): while (have_posts()): the_post();
?>

	<?php if(get_post_meta(get_the_ID(),'set_page_as',true)=="default_wide" || get_post_meta(get_the_ID(),'set_page_as',true) == false):?>
		<?php if ( has_post_thumbnail() && get_post_meta( $post->ID ,'enable_image_header' , true) == 'yes') : // Check if Thumbnail exists ?>
			<section id="blur-bg" class="text-center">
				<div class="article-hero-slider"></div>
				<div class="article-hero-img-cont">
					<div class="article-hero-img text-center">
						<?= the_post_thumbnail( 'full',array('id'=>'srouce-image-hero','class'=>'feat-img') ); ?>
					</div>
				</div>
			</section>
		<?php endif; ?>
	  	<div class="conatiner"> 
			<?php floating_menu_print(); ?>
		</div>
		<div class="floating-container-1">
			<div class="container gallery-container">
			    <div class="eco-mag-text t5-text-heading-2">
			        <h1 class="section-heading main-title"><?=the_title()?></h1>
			        <?php $subtitle = get_post_meta(get_the_ID(), 'sub_title', true);?>
			        <p class=" t5-text-heading-c sub-title"><?php if (isset($subtitle)) {  echo $subtitle;  } ?></p>
			    </div>
				<?php if(get_post_meta( get_the_ID(), "disable_social_link", true )[0] != 'yes'):?>	
				<div class="share-links vertical-links">
	            	<div class="vertical-links-text">
					    <h1 class="share-count-bold"><?= $result->share->share_count ?></h1>
					    <p class="share-count-label">condivisioni</p>
	            	</div>

					<div class="social-media-share-button">

						<div class="share-buttons"><a href="<?= 'https://facebook.com/sharer/sharer.php?u='.urlencode(get_permalink()) ?>" 
							onclick="window.open('<?= 'https://facebook.com/sharer/sharer.php?u='.urlencode(get_permalink()) ?>','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"
						><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/fb.png"></a></div>

						<div class="share-buttons"><a href="https://twitter.com/share?url=<?=urlencode(get_permalink()) ?>"
							onclick="window.open('https://twitter.com/share?url=<?=urlencode(get_permalink()) ?>','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
						<img src="<?php echo get_bloginfo( 'template_directory' );?>/img/tw.png"></a></div>
						<div class="share-buttons"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=urlencode(get_permalink()) ?>"><img width="100%" src="<?php echo get_bloginfo( 'template_directory' );?>/img/in-1.png"></a></div>
					</div>
				</div>
				<?php endif; ?>
			    <div class="page-content">
					<?php echo the_content();?>
				</div>


			</div>
		</div>
	
	
	
	<?php elseif(get_post_meta(get_the_ID(),'set_page_as',true) == "cat_list"):?>
		<div class="container">
			<?php echo the_content();?>
		</div>
	<?php elseif(get_post_meta(get_the_ID(),'set_page_as',true) == "stampa"):?>
	<h1 class="text-center section-heading" id="tabs_target"><?php the_title(); ?></h1>
    
				
    
    <div class="container tabs-container">
        <div class="bs-example tab-pane">
            <img class="hide-img" id="fileb" src="<?php echo get_bloginfo( 'template_directory' );?>/img/file-b.png">
            <img class="hide-img" id="filea" src="<?php echo get_bloginfo( 'template_directory' );?>/img/file.png">
            <?php 
				$floating_menu = get_post_meta( get_the_ID(), 'floating_menu_selected', true);
				if($floating_menu)
					get_template_part('floating_menu');
					
			?>
            <?php $subtitle = get_post_meta(get_the_ID(), 'sub_title', true);?>
            <p id="text-tabs-1" class="text-center"><?php if(isset($subtitle)) echo $subtitle; ?></p>
            <div class="" style="">
                <div class="border-right col-sm-12 col-md-9">
                    
                    <?php the_content(); ?>
                </div>
                <div class="col-sm-12 col-md-3">

                    <?php $releases_contacts = (get_option("releases_contacts"))? get_option("releases_contacts") : ''; ?>

                    <?php foreach($releases_contacts as $contact):?>
                        <div class="tabs-contacts">
                            <p class="name"><?= $contact['name']?></p>
                            <p class="pos"><?= $contact['detail']?></p>
                            <p class="tel"> Tel. <?= $contact['number']?></p>
                            <p class="mail"><?= $contact['email']?></p>
                        </div>
                        
                    <?php endforeach; ?>
                    
                
                </div>
            </div>
        </div>
    </div>

	<?php endif;?>	
	
	<?php endwhile;?>
<?php endif;?>
</section>
<?php if(get_post_meta( $post->ID, "enable_comment", true )[0] == 'enabled'):?>
	<!-- <div class="container">
	    <div class="center-hr chrv2">
	       	<span class="center-hr-element">
		        <div class="commenti"><p>commenti</p></div>
	       	</span>
			   
	    </div>
	</div>
	<section>            
		<div class="container text-center">
			<div class="fb-comments" data-href="<?= get_permalink(); ?>" data-numposts="5"></div>
		</div>
	</section> -->
<?php endif; ?>