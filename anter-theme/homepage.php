
<?php if (have_posts()) : while (have_posts()) : the_post(); 
    $main_id =  get_the_ID();
    $p_wide_cap = get_post_meta($post->ID,'p_wide_cap',true);
    $p_sm_1_cap = get_post_meta($post->ID,'p_sm_1_cap',true);
    $p_sm_2_cap = get_post_meta($post->ID,'p_sm_2_cap',true);
    $p_wide_cap_link = get_post_meta($post->ID,'p_wide_cap_link',true);
    $p_sm_1_cap_link = get_post_meta($post->ID,'p_sm_1_cap_link',true);
    $p_sm_2_cap_link = get_post_meta($post->ID,'p_sm_2_cap_link',true);

?>
<input type="hidden" id="is_home" name="is_home" value="HOME">
<div id="overlay_vid_sm" data-disable="<?= (get_option( "control"  ) == "disable") ? "true" : "false"  ?>" style="display:none">
    <i id="close_mini" class="fa fa-times" style="float: right; font-size:18px;"></i>
    <div class="pop_vid_cont"> 
    <div class="show_pop_up" style="position:  absolute; width:  100%; height:  90%;  bottom:0;"></div> 
    <?php
    $matchesx = array();
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", get_option("homepagepopup_url"), $matchesx); ?>
    <iframe id="mute_vid" width="320px" height="180px" src="https://www.youtube.com/embed/<?= $matchesx[0] ?>?autoplay=1&mute=1" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
    </div>
</div>

<style>#overlay_vid_sm {
    /* position: fixed;
    bottom: 90px;
    right: 10px;
    z-index: 999; */
    display:none;
}
</style>

<section id="main-1">
    <div class="container-fluid clearfix">
        <div class="main-slider-1 clearfix">
            <div id="featured_slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php $carousel_id = get_post_meta( $main_id, 'selected_carousel', true ); ?>
                    <?php 
                        $slides = get_post_meta($carousel_id,'cat_list_slides',true);
                        foreach($slides as $key => $slide):
                    ?>
                        <div class="featured-slide item <?= ($key == 0)? 'active' : ''; ?>">
                            <a href="#"></a>    
                            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                            <img class="hide-img" src="<?= wp_get_attachment_url( $slide['image'] ); ?>">
                            <div class="abosulte-text-slider">
                                  <a href="<?= $slide['link'] ?>"><h1 class="cat-title"><?=  $slide['title']  ?></h1></a>
                            </div>
                        </div>
                    <?php endforeach; ?> 
                    <a class="left carousel-control" href="#featured_slider" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#featured_slider" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="main-slider-right-content clearfix">
            <div class="panel-1 top-img-a featured-image-bg">
                <img  class="hide-img" src="<?= wp_get_attachment_image_src(get_post_meta($main_id,'p_wide',true), 'full')[0]; ?>">                                                            
                <a href="<?= $p_wide_cap_link ?>"><h3 class="panel-h3"><?= $p_wide_cap ?> </h3></a>
            </div><div class="panel-2 clearfix">
                <div class="panel-2-a top-img-a featured-image-bg">
                    <img  class="hide-img" src="<?= wp_get_attachment_image_src(get_post_meta($main_id,'p_sm_1',true), 'full')[0]; ?>">
                    <a href="<?= $p_sm_1_cap_link ?>"><h3 class="panel-h3"><?= $p_sm_1_cap ?> </h3></a>
                </div><div class="panel-2-b top-img-a featured-image-bg">
                    <img class="hide-img" src="<?= wp_get_attachment_image_src(get_post_meta($main_id,'p_sm_2',true), 'full')[0]; ?>">
                    <a href="<?= $p_sm_2_cap_link ?>"><h3 class="panel-h3"><?= $p_sm_2_cap ?> </h3></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top:75px;">
        <div id="post_loop" class="row row-eq-height">
            <?php
                $counter = 1;
                $small_ad_attachment = get_post_meta($main_id,'sm_ad',true);    
                $large_ad_attachment = get_post_meta($main_id,'lg_ad',true);
                $sm_ad_link = get_post_meta($main_id,'sm_ad_link',true);
                $lg_ad_link = get_post_meta($main_id,'lg_ad_link',true);
                $front_page_elements = get_option("theme_name_front_page_elements");
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => ($small_ad_attachment)? 10 : 11,
                
                );
                $show_wide = 5;
                $the_query = new WP_Query($args);
                
            ?>

            <?php if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post();?>
                <div class="col-xs-12 col-sm-6 col-md-4 remove-padd active">
                    <div class="card-container <?=has_tag('featured') ? 'special' : ''?>">
                        <div class="card-img-container">
                        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                            <a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail('thumb-img-1x', array('class' => '')  ); // Declare pixel size you need inside the array ?>
                            </a>
                        <?php else:?>
                            <a href="<?php the_permalink(); ?>">
								<img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                            </a>
						<?php endif; ?>
                            <div>
                                <a href="<?= get_category_link(get_the_category()[0]->term_id); ?>"><p class="card-img-text"><?=get_the_category()[0]->cat_name;?></p></a>
                            </div>
                        </div>
                        <div class="card-content">
                            <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                <h3 class="card-heading"><?php the_title();?></h3>
                            </a>
                            <p class="card-text">
                                <?php echo (get_the_excerpt()); ?>
                            </p>
                        </div>
                    </div>
                </div>

                    <?php if ($counter == 2): ?>
                        <?php dynamic_sidebar( 'weather_widget' ); ?>
                    <?php elseif ($counter == 4 && $small_ad_attachment): ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd" style="padding-top:70px;  text-align: center;">
                            <a href="<?= $sm_ad_link  ?>"><img src="<?=wp_get_attachment_url($small_ad_attachment)?>"></a>
                        </div>
                    <?php elseif ($counter == $show_wide && $large_ad_attachment != ''): ?>
                        <div class="col-xs-12 text-center billboard-cont">
                            <a href="<?= $lg_ad_link  ?>"><img src="<?=wp_get_attachment_url($large_ad_attachment)?>"></a>
                        </div>
                    <?php endif;?>
                    
                    <?php $counter++;endwhile;?>

            </div>
        </div>
    </div>
    <?php  if ($the_query->max_num_pages > 1):?>
        <div class="container" id="load_more">
            <div class="center-hr">
                <span class="center-hr-element">
                    <button class="misha_loadmore btn btn-v-1">MOSTRA ALTRI</button>
                </span>
            </div>
        </div>
    <?php endif; ?>
</section>

<?php else: ?>


    <article>
        <h2><?php _e('Sorry, nothing to display.', 'html5blank');?></h2>
    </article>

<?php endif;?>

<?php endwhile;?>

<?php endif;?>