<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    

	<title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' '; } ?></title>
    
    <link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Work+Sans:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link href="<?php echo get_bloginfo( 'template_directory' );?>/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/flexslider.min.css" />
    <link href="<?php echo get_bloginfo( 'template_directory' );?>/assets/css/responsive.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>
    
    <script type="text/javascript" src="<?php echo get_bloginfo( 'template_directory' );?>/js/script.js"></script>
    <?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head();?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=1671129072925692';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</head>

<body class="<?= implode(' ', get_body_class()) ?>">
    <header>
        <nav id="main_menu" class="navbar navbar-inverse white-nav">
            <div class="container">
                <div class="navbar-header">
                    
                    <a href="<?= esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/logo/logo2.png"></a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <i class="fa fa-bars"></i>
                    </button>
                    
                </div>
                <div class="collapse navbar-collapse container" id="myNavbar">
                    <?php
                            wp_nav_menu( array( 
                                'theme_location' => 'main-menu', 
                                'container' => 'ul',
                                'menu_class'=> 'nav navbar-nav navbar-center'
                                ) 
                            ); 
                    ?>
                </div>
            </div>
        </nav>
        <div class="container banner">
            <div id="assoc" class="align-elem-left">
                <div class="">
    
                        <span id="assoc-count"><?php member_counter() ?></span>
                    <p>Associati</p>
                </div>
            </div>
            <div class="main-logo">
                <a href="<?= esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/logo/logo2.png"></a>
            </div>
            <div class="align-elem-right left-banner">
                <div class="social-media-links-banner">
                    <p>SEGUICI SU</p>
                    <ul class="social-meida-icons-banner">
                        <li class="fb"><a href="https://www.facebook.com/associazioneanteritalia"><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/fb.png"></a></li>
                        <li class="yt"><a href="https://www.youtube.com/channel/UCnUQELRfFQPfPAmVDZzoR9A/featured"><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/yt.png"></a></li>
                        <li class="tw"><a href="https://twitter.com/anter_italia"><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/tw.png"></a></li>
                    </ul>
                </div>
                <div id="nwg">
                    <p>FOUNDER</p>
                    <a href="http://www.nwgenergia.it/" target="_blank"><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/logo/NWG.png"></a>
                </div>
            </div>
        </div>
        <div class="hide-scroll">
            <div class="scrollable">
                <div class="scroll-contorl left-c">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </div>
                <div class="scroll-contorl right-c">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </div>
                <div id="sub-menu" class="container">
                    
                        <?php
                            wp_nav_menu( array( 
                                'theme_location' => 'category-menu', 
                                'container' => 'ul',
                                'menu_class'=> 'nav navbar-nav'
                                ) 
                            ); 
                        ?>
                    
                </div>
            </div>
        </div>
    </header>
