<?php
/**
 * Template Name:  Template 7
 */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="main-content" class="main-content">


<?php $second_featured_img = get_post_meta(get_the_ID(), 'second_featured_img', true); ?>

<?php if ( has_post_thumbnail() && get_post_meta( $post->ID ,'enable_image_header' , true) == 'yes') : // Check if Thumbnail exists ?>
	

<section id="blur-bg" class="text-center">
        <div class="article-hero-slider"><img src=""></div>
        <div class="article-hero-img-cont">
        <div class="article-hero-img container text-center">
			<img id="srouce-image-hero" src="<?= get_the_post_thumbnail_url(); ?>">
        </div>
    </div>
</section>
<?php endif; ?>
<section id="article-main-container">

<div class="container">
			
			<?php 
				$result = get_share_count(get_permalink());
				$floating_menu = get_post_meta( get_the_ID(), 'floating_menu_selected', true);
				if($floating_menu)
					get_template_part('floating_menu')
					
			?>
	<div id="main-article" class="article-view  t7 clearfix">
        <div class="post-header">
				<h1 class="post-title main-title"><?php the_title(); ?></h1>
				<?php $subtitle = get_post_meta(get_the_ID(), 'sub_title', true);?>
				<p class="post-short-desc sub-title"><?php if(isset($subtitle)) echo $subtitle; ?></p>
		</div>
		<div class="share-links vertical-links">
            <div class="vertical-links-text">
			    <h1 class="share-count-bold"><?= $result->share->share_count ?></h1>
			    <p class="share-count-label">condivisioni</p>
            </div>
			<div class="social-media-share-button">
                
			<div class="share-buttons"><a href="<?= 'https://facebook.com/sharer/sharer.php?u='.urlencode(get_permalink()) ?>" 
			onclick="window.open('<?= 'https://facebook.com/sharer/sharer.php?u='.urlencode(get_permalink()) ?>','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"
			><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/fb.png"></a></div>
		   
			<div class="share-buttons"><a href="https://twitter.com/share?url=<?=urlencode(get_permalink()) ?>"
			onclick="window.open('https://twitter.com/share?url=<?=urlencode(get_permalink()) ?>','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
			<img src="<?php echo get_bloginfo( 'template_directory' );?>/img/tw.png"></a></div>
			<div class="share-buttons"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=urlencode(get_permalink()) ?>"><img width="100%" src="<?php echo get_bloginfo( 'template_directory' );?>/img/in-1.png"></a></div>
			</div>
		</div>
		<div class="t7-content">
        <?php echo the_content();?>
			

		</div>
	</div>
</div>
</section>

<div class="container">
    <div class="center-hr chrv2">
       	<span class="center-hr-element">
	        <div class="commenti"><p>commenti</p></div>
       	</span>
    </div>
</div>


<section>            
	<div class="container text-center">
		<img src="<?php echo get_bloginfo( 'template_directory' );?>/img/comments.jpg">
	</div>
</section>


<?php endwhile; ?>
<?php endif; ?>



</div>
<?php

get_footer();


?>

