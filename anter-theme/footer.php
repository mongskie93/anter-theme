    <footer>
        <div class="container container-v2">
            <div class="row" id="footer-widgets">
                <div class="col-xs-12 col-sm-12 col-md-12 social-buttons social-buttons-title">
                    <p>Seguici su</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 social-buttons">
                    <a href="https://www.facebook.com/associazioneanteritalia" class="fb">
                        <i class="fa fa-facebook-official"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCnUQELRfFQPfPAmVDZzoR9A/featured" class="youtube">
                        <i class="fa fa-youtube-play"></i>
                    </a>
                    <a href="https://twitter.com/anter_italia" class="twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="widget-container bg-green">
                        <h3 class="text-white">ASSOCIAZIONE <br>NO PROFIT</h3>
                        <p class="text-white">Per promuovere e tutelare le energie rinnovabili in Italia. L'unica nel settore delle energie rinnovabili ad essere dalla parte della domanda e non dell'offerta.</p>
                        <a href="tel:+39057434805" class="btn text-white"><img id="phone-btn-img" src="<?php echo get_bloginfo( 'template_directory' );?>/img/phone.png">+39 0574 34805</a>
                        <a href="mailto:segreteria@anter.info" class="btn text-white"><img id="mail-btn-img" src="<?php echo get_bloginfo( 'template_directory' );?>/img/mail.png">segreteria@anter.info</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="widget-container sky-bg ">
                        <h3 class="" style="margin-bottom: 10px;">AMBASCIATORI<br>ANTER</h3>
                        <img id="map-icon" src="<?php echo get_bloginfo( 'template_directory' );?>/img/map2.png">
                        <a href="/anter/ambasciatori/" class="btn text-white gold-border">Trova quello più vicino a te!</a>
                    </div>
                    <a href="http://anteritalia.org/areariservataambasciatori/">
                        <div class="widget-container blug-drk-bg ">
                            <p class="text-white">Area Riservata</p>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="widget-container yellow-bg">
                        <span id="assoc-bot" class="text-white"><?php member_counter(); ?></span>
                        <h4 class="text-white">ASSOCIATI SU TUTTO<br>IL TERRITORIO NAZIONALE</h4>
                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/logow.png">
                        <a href="/anter/diventa-socio/" class="btn text-center text-white white-border-1">Sostienici. Diventa socio</a>
                        <a href="/anter/5x1000/" class="btn text-center  text-white white-border">Dona l'5X1000</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container text-center text-9d">

        <p>Orario di apertura: <br>
        dal lunedì al venerdì, dalle ore 9:00 alle ore 13:00.</p>

        <p>ANTER Associazione Nazionale Tutela Energie Rinnovabili<br>
        C.F. 91026310473 - P.IVA 01680940473</p>

        <p>Sede Legale e operativa:<br>
        Via Traversa Pistoiese, 83 59100 Prato<br>
        Tel: +39 0574 34805 Fax +39 0574 028311<br>
        Email: <a href="mailto:segreteria@anter.info" target="_top">segreteria@anter.info</a><br>
        <a href="http://anteritalia.org/privacy/">Privacy policy</a>
        </p>
        
            
        </div>
    </footer>
    <?php wp_footer() ?>
    <div id="video_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-vid" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div id="image_modal" class="modal fade" role="dialog" tabindex='-1'>
            <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-vid" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div id="gallery_items" class="carousel-inner">
                          
                        </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                          <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div> 
</body>

</html>