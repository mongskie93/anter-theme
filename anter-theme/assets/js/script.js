$(document).ready(function() {


    $(".square").each(function(index) {
        var url = $(this).children("img").attr("src");
        $(this).css("background", "url(" + url + ") ");
        $(this).css("background-size", "110%");
        $(this).css("background-position", "center");

    });


    $(".article-hero-slider").each(function(index) {
        var url = $("#srouce-image-hero").attr("src");
        $(this).css("background", "url(" + url + ") ");
    });
    $(".nav-tabs > li a").on('click', function() {

        $("#tabs_target").text($(this).text());
    });

    $(window).resize(function() {
        contain_overlap_slider();
    });

    //contain_overlap_slider();
    function contain_overlap_slider() {
        $("#hero-slider").css('height', 'auto');
        var old_height = $("#hero-slider").height();
        console.log(old_height);
        $("#hero-slider").height(old_height - 5);
        $(".panel-2-b img").css('height', 'auto');
        var old_height_a = $(".panel-2-b img").height();
        console.log(old_height_a);
        $(".panel-2-a img").height(old_height_a);

        $(".main-slider-right-content").height($(".main-slider-1").height());

    }


    var el1 = document.getElementsByClassName('left-c')[0],
        elClone = el1.cloneNode(true);

    el1.parentNode.replaceChild(elClone, el1);


    var el2 = document.getElementsByClassName('right-c')[0],
        elClone = el2.cloneNode(true);

    el2.parentNode.replaceChild(elClone, el2);

    $('header .scrollable').scroll(function() {

        if ($('header .scrollable').scrollLeft() <= 0) {
            $('.left-c').css('display', 'none');
        } else {
            $('.left-c').css('display', 'block');
        }


        if ($('header .scrollable').scrollLeft() + $('header .scrollable').width() >= $('#sub-menu').width() + 30) {
            $('.right-c').css('display', 'none');
        } else {
            $('.right-c').css('display', 'block');
        }

    });

    $('.left-c').click(function() {
        $('header .scrollable').animate({
            scrollLeft: $('header .scrollable').scrollLeft() - 200
        });
    });

    $('.right-c').click(function() {
        $('header .scrollable').animate({
            scrollLeft: $('header .scrollable').scrollLeft() + 200
        });
    });




    $(window).scroll(function() {

        if ($(window).scrollTop() >= $('#top_content').offset().top - 150 && $(window).scrollTop() < $('#content_1st').offset().top - 150) {

            $('.nav-v3 a').removeClass("active");
            $("a[href='#top_content']").addClass("active");
            console.log("1---");

        }

        if ($(window).scrollTop() >= $('#content_1st').offset().top - 150 && $(window).scrollTop() < $('#content_2nd').offset().top - 150) {

            $('.nav-v3 a').removeClass("active");
            $("a[href='#content_1st']").addClass("active");
            console.log("2---");
        }

        if ($(window).scrollTop() >= $('#content_2nd').offset().top - 150 && $(window).scrollTop() < $('#content_3rd').offset().top - 150) {

            $('.nav-v3 a').removeClass("active");
            $("a[href='#content_2nd']").addClass("active");
            console.log("3---");
        }

        if ($(window).scrollTop() >= $('#content_3rd').offset().top - 150 && $(window).scrollTop() < $('#gallery').offset().top - 100) {

            $('.nav-v3 a').removeClass("active");
            $("a[href='#content_3rd']").addClass("active");
            console.log("4---");
        }

        if ($(window).scrollTop() >= $('#gallery').offset().top - 150) {
            $('.nav-v3 a').removeClass("active");
            $("a[href='#gallery']").addClass("active");
            console.log("5---");
        }

    });


    $('#video_modal').on('shown.bs.modal', function(e) {

        $('#video_modal .modal-body').html($("#view_video").html());
    });

    var file = $(".file");
    file.mouseover(function() {
        var img = $(this).find('img');
        img.attr('src', 'img/file-b.png');

    });
    file.mouseleave(function() {
        var img = $(this).find('img');
        img.attr('src', 'img/file.png');

    });

    //active menu item
    var topMenu = $(".nav-v3"),
        topMenuHeight = topMenu.outerHeight() + 15,
        menuItems = topMenu.find("a"),
        scrollItems = menuItems.map(function() {
            var item = $($(this).attr("href"));
            if (item.length) { return item; }
        });

    $(window).scroll(function() {
        var fromTop = $(this).scrollTop() + topMenuHeight;

        var cur = scrollItems.map(function() {
            if ($(this).offset().top < fromTop)
                return this;
        });

        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        menuItems
            .parent().removeClass("active")
            .end().filter("[href='#" + id + "']").parent().addClass("active");
    });
    //



    // Instantiate the Bootstrap carousel
    $('.multi-item-carousel').carousel({
        interval: false
    });

    // for every slide in carousel, copy the next slide's item in the slide.
    // Do the same for the next, next item.
    $('.multi-item-carousel .item').each(function() {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });















});