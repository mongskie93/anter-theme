$(document).ready(function () {



    if ($(window).width() < 1024) {
        $("#overlay_vid_sm").html("");
        if ($("#overlay_vid_sm").attr("data-disable") == "false") {
            var video_id = "";
            if ($("#is_home").val() == "HOME") {
                $.ajax({
                    url: "/wp-admin/admin-ajax.php?action=get_popup_video"
                }).done(function (data) {
                    video_id = data;
                    if (getCookie("visited") != "Yes") {
                        console.log("IF OK");
                        $('#video_modal').modal('show');
                        $('#video_modal').on('shown.bs.modal', function (e) {
                            $('#video_modal .modal-body').html('<iframe width="1024px" height="576px" src="https://www.youtube.com/embed/' + video_id + '?" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen> </iframe>');

                        });
                        $("#video_modal").on("hidden.bs.modal", function () {
                            $("#video_modal .modal-body").html("");
                            $('#video_modal .modal-title').text("");
                            $('#video_modal .modal-date').text("");
                        });
                        $(".modal-backdrop").addClass("background", "rgba(0,0,0,0.4)");
                    }
                    document.cookie = "visited=Yes";
                });

            }
        }
    }

    if ($("#overlay_vid_sm").attr("data-disable") == "false") {

        function show_large_pop_up(video_id) {
            if ($(window).width() < 1024) {
                return;
            }
            $('#video_modal').modal('show');
            $('#video_modal').on('shown.bs.modal', function (e) {
                $('#video_modal .modal-body').html('<iframe id="xorign" width="1024px" height="576px" src="https://www.youtube.com/embed/' + video_id + '?autoplay=1" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>');
                window.onmessage = function(event) {
                    event.source.postMessage(document.body.innerHTML, event.origin);
                };
                
                
                
                
                
                document.getElementById('xorign').contentWindow.postMessage('','*');

            });
            $("#video_modal").on("hidden.bs.modal", function () {
                $("#video_modal .modal-body").html("");
                $('#video_modal .modal-title').text("");
                $('#video_modal .modal-date').text("");
            });
            $(".modal-backdrop").addClass("background", "rgba(0,0,0,0.4)");

        }
        var video_id = "";
        console.log(window.location.href);
        if ($("#is_home").val() == "HOME") {
            $.ajax({
                url: "/wp-admin/admin-ajax.php?action=get_popup_video"
            }).done(function (data) {
                
                video_id = data;
                if (getCookie("visited") != "Yes") {
                    show_large_pop_up(data);
                } else {
                    $("#overlay_vid_sm").html("");
                    $("#overlay_vid_sm").hide();
                    // $("#overlay_vid_sm").show();
                    $(".show_pop_up").click(function () {
                        $("#overlay_vid_sm").hide();
                        
                        show_large_pop_up(data);
                    });
                    $(".show_pop_up").hover(function () {
                        $("#overlay_vid_sm").hide();
                        $("#overlay_vid_sm").html("");
                        show_large_pop_up(data);
                    });
                }
                document.cookie = "visited=Yes";
            });

            $("#close_mini").click(function () {
                $("#overlay_vid_sm").hide();
                $("#overlay_vid_sm").html("");
            });


        }

    }






    //$(".col-xs-12.col-sm-6.col-md-4.remove-padd.weather-item").addClass("active").removeClass("weather-item");
    if ($("#content_1st").length !== 0) {
        $(window).scroll(function () {

            if ($(window).scrollTop() >= $('#top_content').offset().top - 150 && $(window).scrollTop() < $('#content_1st').offset().top - 150) {

                $('.nav-v3 a').removeClass("active");
                $("a[href='#top_content']").addClass("active");
            }

            if ($(window).scrollTop() >= $('#content_1st').offset().top - 150 && $(window).scrollTop() < $('#content_2nd').offset().top - 150) {

                $('.nav-v3 a').removeClass("active");
                $("a[href='#content_1st']").addClass("active");
            }

            if ($(window).scrollTop() >= $('#content_2nd').offset().top - 150 && $(window).scrollTop() < $('#content_3rd').offset().top - 150) {

                $('.nav-v3 a').removeClass("active");
                $("a[href='#content_2nd']").addClass("active");
            }

            if ($(window).scrollTop() >= $('#content_3rd').offset().top - 150 && $(window).scrollTop() < $('#gallery').offset().top - 100) {

                $('.nav-v3 a').removeClass("active");
                $("a[href='#content_3rd']").addClass("active");
            }

            if ($(window).scrollTop() >= $('#gallery').offset().top - 150) {
                $('.nav-v3 a').removeClass("active");
                $("a[href='#gallery']").addClass("active");
            }

        });
    }
    var full = 0;
    $('.inline-list-f').each(function () {
        full = full + 480;
    });
    console.log(full);
    $('#slider-thumbs').width(full + 100);
    $('#myCarousel').carousel({
        interval: false
    });

    // handles the carousel thumbnails


    // when the carousel slides, auto update
    $('#myCarousel').on('slid.bs.carousel', function (e) {
        var id = $('.item.active').data('slide-number');
        id = parseInt(id / 10);
        $('[id^=carousel-selector-]').removeClass('selected');
        $('[id=carousel-selector-' + id + ']').addClass('selected');


    });
    // MENU HOVER

    $(document).ready(function () {
        var currentHeight = $("#myCarousel .carousel-inner .item.active img").height();
        $("#myCarousel .carousel-control span").css("top", ($(document).width() > 903) ? currentHeight / 2 : currentHeight / 2 + 160);
        setTimeout(function () {
            var currentHeight = $("#myCarousel .carousel-inner .item.active img").height();
            $("#carousel-bounding-box #myCarousel .carousel-control span").css("top", currentHeight / 2);
        }, 500);
    });
    $('#myCarousel').on('slid.bs.carousel', function (e) {

        var currentHeight = $("#myCarousel .carousel-inner .item.active img").height();
        $("#myCarousel .carousel-control span").css("top", ($(document).width() > 903) ? currentHeight / 2 : currentHeight / 2 + 160);

        $("#image_modal #myCarousel .carousel-control span").css("top", currentHeight / 2);
        $("#carousel-bounding-box #myCarousel .carousel-control span").css("top", currentHeight / 2);

    });


    $('li.menu-item').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
    // SQUARE IMAGES

    function load_squares() {
        $(".square").each(function (index) {
            var url = $(this).find("img").attr("src");
            $(this).css("background", "url(" + url + ") ");
            $(this).css("background-size", "110%");
            $(this).css("background-position", "center");

        });
    }
    load_squares();
    $(".article-hero-slider").each(function (index) {
        var url = $("#srouce-image-hero").attr("src");
        $(this).css("background", "url(" + url + ") ");
    });

    $(".featured-slide").each(function (index) {
        var url = $(this).children("img").attr("src");
        $(this).css("background", "url(" + url + ") ");
        $(this).css("background-size", "110%");
        $(this).css("background-position", "center");

    });
    $(".featured-image-bg").each(function (index) {
        var url = $(this).children("img").attr("src");
        $(this).css("background", "url(" + url + ") ");
        $(this).css("background-size", "cover");
        $(this).css("background-position", "center");

    });


    $(window).resize(function () {
        contain_overlap_slider();
    });

    //contain_overlap_slider();
    function contain_overlap_slider() {
        $("#hero-slider").css('height', 'auto');
        var old_height = $("#hero-slider").height();
        console.log(old_height);
        $("#hero-slider").height(old_height - 5);
        $(".panel-2-b img").css('height', 'auto');
        var old_height_a = $(".panel-2-b img").height();
        console.log(old_height_a);
        $(".panel-2-a img").height(old_height_a);

        $(".main-slider-right-content").height($(".main-slider-1").height());

    }

    $('.right-c').click(function () {
        console.log('test');
        var margin_left = $('.scrollable').css('margin-left').replace('px', '');
        console.log(margin_left);

        margin_left = parseInt(margin_left) - 70;
        console.log(margin_left);
        if (margin_left < 0) {
            $('.left-c').show();
        }
        if (margin_left < -320) {
            $('.right-c').hide();
            console.log("test");

        }
        $(".scrollable").animate({ 'margin-left': margin_left }, 'fast');

    });


    $('.left-c').click(function () {
        console.log('test');
        var margin_left = $('.scrollable').css('margin-left').replace('px', '');
        console.log(margin_left);

        margin_left = parseInt(margin_left) + 70;
        console.log(margin_left);
        if (margin_left > 0) {
            $('.left-c').hide();
            $(".scrollable").animate({ 'margin-left': 0 }, 'fast');
            return 1;
        }

        if (margin_left >= -490) {
            $('.right-c').show();
            console.log("test");
        }

        $(".scrollable").animate({ 'margin-left': margin_left }, 'fast');

    });
    $('.video-thumb').click(function () {
        var video_id = $(this).find('.youtube_url').val();
        var video_title = $(this).find('.gallery-card-text').text();
        var video_date = $(this).find('.gallery-card-date').text();

        $('#video_modal').modal('show');
        $('#video_modal').on('shown.bs.modal', function (e) {
            $('#video_modal .modal-body').html('<iframe width="1280px" height="720px" src="https://www.youtube.com/embed/' + video_id + '" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>');
            $('#video_modal .modal-title').text(video_title);
            $('#video_modal .modal-date').text(video_date);
        });
    });

    function load_modals() {
        $('.Video-banner').click(function () {
            var video_id = $(this).find('input').val();
            $('#video_modal').modal('show');
            $('#video_modal').on('shown.bs.modal', function (e) {
                $('#video_modal .modal-body').html('<iframe width="1280px" height="720px" src="https://www.youtube.com/embed/' + video_id + '" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>');
                $('#video_modal .modal-title').text("");
                $('#video_modal .modal-date').text("");
            });
        });

        $("#video_modal").on("hidden.bs.modal", function () {
            $("#video_modal .modal-body").html("");
            $('#video_modal .modal-title').text("");
            $('#video_modal .modal-date').text("");
        })

        $('.image-modal-toggle').click(function () {
            var imaga_src = $(this).find('img').attr('asrc');
            var data_id = $(this).find('img').attr('data-id');
            var video_title = $(this).find('.gallery-card-text').text();
            var video_date = $(this).find('.gallery-card-date').text();

            ;


            var items = '';
            var active = 0;
            $(this).closest('.gallery-container').find('.eco-mag-img-container').each(function () {
                var item = $(this).find('img').attr('asrc');
                var data_id_new = $(this).find('img').attr('data-id');
                if (data_id_new == data_id && active == 0) {
                    active = 1;
                    var head = '<div class="item active">';
                } else {
                    var head = '<div class="item">';
                }


                var tail = '</div>';




                items = items + head + '<img width="100%" src="' + item + '">' + tail;
            });
            $('#image_modal').on('shown.bs.modal', function (e) {
                $('#image_modal .modal-body #gallery_items').html(items);
                setTimeout(function () {
                    var currentHeight = $("#image_modal #myCarousel .item.active img").height();
                    $("#image_modal #myCarousel .carousel-control span").css("top", currentHeight / 2);
                }, 1000);
            });

            $('#image_modal').modal('show');
            console.log(items);
        });

        $("#image_modal").on("hidden.bs.modal", function () {
            $('#image_modal .modal-body #gallery_items').html("");
            $('#image_modal .modal-title').text("");
            $('#image_modal .modal-date').text("");
        });
    }
    load_modals();
    var file = $(".file");
    file.mouseover(function () {
        var img = $(this).find('img');
        img.attr('src', $('#fileb').attr('src'));

    });
    file.mouseleave(function () {
        var img = $(this).find('img');
        img.attr('src', $('#filea').attr('src'));

    });


    // Instantiate the Bootstrap carousel
    $('.multi-item-carousel').carousel({
        interval: false
    });

    // for every slide in carousel, copy the next slide's item in the slide.
    // Do the same for the next, next item.
    $('.multi-item-carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });








    $('.next-vid-page').click(function () {
        var post_id = $(this).attr('data-gallery-id');
        var page_num = $(this).attr('data-gallery-page');
        var data_url = $(this).attr('data-url');
        var pages = $(this).attr('last-page');


        $(this).attr('data-gallery-page', parseInt(page_num) + 1);

        var url = data_url + '?action=load_next_vid_set&&post=' + post_id + '&&page_num=' + page_num;
        $.get(url, function (data) {
            $('.galc-' + post_id).append(data);
            load_squares();
            load_modals();


        });
        if (page_num == pages - 1) {
            $('.load-' + post_id).fadeOut();
        }

    });


    $('.next-pot-page').click(function () {
        var post_id = $(this).attr('data-gallery-id');
        var page_num = $(this).attr('data-gallery-page');
        var data_url = $(this).attr('data-url');
        var pages = $(this).attr('last-page');


        $(this).attr('data-gallery-page', parseInt(page_num) + 1);

        var url = data_url + '?action=load_photo_gallery&&post=' + post_id + '&&page_num=' + page_num;
        console.log(url);
        $.get(url, function (data) {
            $('.append-' + post_id).append(data);
            load_squares();
            load_modals();

        });
        if (page_num == pages - 1) {
            $('.load-' + post_id).fadeOut();
        }


    });





    $('li.menu-item').hover(function () {

        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    if ($('#lightSlider').length != 0) {
        var autoplaySlider = $('#lightSlider').lightSlider({
            auto: true,
            loop: true,
            item: 2,
            pauseOnHover: true,
            onBeforeSlide: function (el) {
                $('#current').text(el.getCurrentSlideCount());
            }
        });
        $('#total').text(autoplaySlider.getTotalSlideCount());
    }
    $("a .cat-name").click(function () {
        window.location = $(this).parent().attr("href");
        return false;
    });
    $(".featured-slide").click(function () {
        window.location = $(this).find("a:last-child").attr("href");
        return false;
    });

    $(".top-img-a").click(function () {
        window.location = $(this).find("a:last-child").attr("href");
        return false;
    });




    $(".nav-v3>li>a").click(function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 120
        }, 500);
    });


    $(document).keydown(function (e) {
        if (e.keyCode === 37) {
            // Previous
            $(".carousel-control.left").click();
            return false;
        }
        if (e.keyCode === 39) {
            // Next
            $(".carousel-control.right").click();
            return false;
        }
    });


    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

});

$(window).scroll(function () {
    var sticky = $('#floating-menu-1'),
        scroll = $(window).scrollTop();
    var scroll_offset = 350;
    if ($('#blur-bg').length === 1) {
        scroll_offset = 350 + $('#blur-bg').height();
    }
    if (scroll >= scroll_offset) {
        sticky.addClass('fixed');
        if (sticky.length === 1) {

            $('#main-article').css('margin-top', 215);
            $('.floating-container-1').css('margin-top', 215);

        }

    } else {
        sticky.removeClass('fixed');
        $('#main-article').css('margin-top', '0px');
        $('.floating-container-1').css('margin-top', 0);
    }

});


$(document).ready(function () {

    $("#scrollable").wrap("<div class='scrollable-wrapper'></div>");

    $(".scrollable-wrapper").append("<div class='arrow-slid left'></div>");

    $(".scrollable-wrapper").append("<div class='arrow-slid right'></div>");

    $('.scrollable-wrapper #scrollable').scroll(function () {

        if ($('.scrollable-wrapper #scrollable').scrollLeft() <= 0) {
            $('.arrow-slid.left').css('display', 'none');
        } else {
            $('.arrow-slid.left').css('display', 'block');
        }
        if ($('.scrollable-wrapper #scrollable').scrollLeft() + $('.scrollable-wrapper #scrollable').width() >= $('#slider-thumbs').width() + 30) {
            $('.arrow-slid.right').css('display', 'none');
        } else {
            $('.arrow-slid.right').css('display', 'block');
        }
    });

    $('.arrow-slid.left').click(function () {
        $('.scrollable-wrapper #scrollable').animate({
            scrollLeft: $('.scrollable-wrapper #scrollable').scrollLeft() - 200
        });
    });
    $('.arrow-slid.right').click(function () {
        $('.scrollable-wrapper #scrollable').animate({
            scrollLeft: $('.scrollable-wrapper #scrollable').scrollLeft() + 200
        });
    });

    $("#slider-thumbs .inline-list-f").each(function (index) {
        $(this).attr("data-target", "#myCarousel");
        $(this).attr("data-slide-to", index);


    });
    $("#myCarousel .item").each(function (index) {
        $(this).attr("slide-number", index);
    });
    $(document).ready(function () {

        $("#slider-thumbs .inline-list-f a").removeClass("selected");

        $("#slider-thumbs .inline-list-f[data-slide-to='0'] a").addClass("selected");

        $(".scrollable-wrapper #scrollable").animate({
            scrollLeft: 90
        });

        $('.arrow-slid.left').css('display', 'none');

    });
});
$(document).ready(function () {
    var currentHeight = $("#myCarousel .carousel-inner .item.active img").height();
    $("#myCarousel .carousel-control span").css("top", ($(document).width() > 903) ? currentHeight / 2 : currentHeight / 2 + 160);
});