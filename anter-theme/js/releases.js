jQuery(function($) {
    /*
     * Select/Upload image(s) event
     */
    $('body').on('click', '.pdf_release_btn', function(e) {
        e.preventDefault();

        var button = $(this),
            custom_uploader = wp.media({
                title: 'Select/Upload Files',
                library: {
                    // uncomment the next line if you want to attach image to the current post
                    // uploadedTo : wp.media.view.settings.post.id, 
                    type: 'application/pdf'
                },
                button: {
                    text: 'Use this file' // button label text
                },
                multiple: false // for multiple image selection set to true
            }).on('select', function() { // it also has "open" and "close" events 
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $(button).removeClass('button').html('<div style="display:inline-block;box-sizing:border-box;padding:10px; border:1px solid gray;"><img class="true_pre_image" src="' + $('#img-temp').attr('data-src') + '" style="margin:0 auto;max-width:400px;display:block;" /></div>').next().val(attachment.id).next().show();
                /* if you sen multiple to true, here is some code for getting the image IDs
                var attachments = frame.state().get('selection'),
                    attachment_ids = new Array(),
                    i = 0;
                attachments.each(function(attachment) {
                    attachment_ids[i] = attachment['id'];
                    console.log( attachment );
                    i++;
                });
                */
            })
            .open();
    });

    /*
     * Remove image event
     */
    $('body').on('click', '.remove_files_s', function() {
        $(this).hide().prev().val('').prev().addClass('button').html('Upload File');
        return false;
    });


    $('.add-file').click(function() {
        $('#files').append($('#temp-val').html());

    });
    $('.remove-file').click(function() {
        $(this).parent().parent().remove();
        remove();
    });
    remove();

    function remove() {
        $('.remove-file').click(function() {
            $(this).parent().parent().remove();
        });
    }
});