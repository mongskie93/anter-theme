$(document).ready(function() {
    var video_id = "";
    $.ajax({
        url: "/wp-admin/admin-ajax.php?action=get_popup_video",
      }).done(function(data) {
        video_id = data;
      });
    $('#video_modal').modal('show');
        $('#video_modal').on('shown.bs.modal', function(e) {
            $('#video_modal .modal-body').html('<iframe width="1280px" height="720px" src="https://www.youtube.com/embed/' + video_id + '" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>');
            $('#video_modal .modal-title').text(video_title);
            $('#video_modal .modal-date').text(video_date);
    });
});