
<?php
$menu_logo = get_post_meta(
	get_post_meta( 
		get_the_ID(), 'floating_menu_selected', true), 
		'menu_logo',
		true
	);


?>
<div id="floating-menu-1" class="container gallery-container">
<div class="t5-menus">
	<div class="t5-menu-img-c">
		<img src="<?=wp_get_attachment_url($menu_logo);?>">
	</div>
	<div class="t5-menus-btn-c">
		<ul>
		<?php
			$pages = get_post_meta(
				get_post_meta( 
					get_the_ID(), 'floating_menu_selected', true), 
					'floating_menu_items',
					true
				);

				

			foreach($pages as $page):?>
				<?php 
					$is_active = "";
					if(get_permalink() == get_permalink($page['pages']))
						$is_active = "active";
				?>
				<li class="<?= $is_active ?>"><a href="<?= get_permalink($page['pages']) ?>"><?= $page['name'] ?> </a></li>
				
			<?php endforeach;?>
		</ul>
	</div>
</div>
</div>