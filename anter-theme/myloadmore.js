jQuery(function($) {
    $('.misha_loadmore').click(function() {

        var button = $(this),
            data = {
                'action': 'loadmore',
                'query': misha_loadmore_params.posts, // that's how we get params from wp_localize_script() function
                'page': misha_loadmore_params.current_page
            };
        console.log(data);

        $.ajax({
            url: misha_loadmore_params.ajaxurl, // AJAX handler
            data: data,
            type: 'POST',
            success: function(data) {
                if (data) {
                    console.log(data);
                    $('#post_loop').append(data).show('slow');
                    misha_loadmore_params.current_page++;

                    if (misha_loadmore_params.current_page == misha_loadmore_params.max_page)
                        button.remove(); // if last page, remove the button
                } else {
                    button.remove();
                    $('#load_more').remove(); // if no data, remove the button as well
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
});