
<?php
$menu_logo = get_post_meta(
	get_post_meta( 
		get_the_ID(), 'floating_menu_selected', true), 
		'menu_logo',
		true
	);

$type = get_post_meta(
	get_post_meta( 
		get_the_ID(), 'floating_menu_selected', true), 
		'floating_menu_type',
		true
	);


?>

<div id="floating-menu-1" class="container gallery-container <?= $type ?>">
<?php if($type == 'with_logo'):?>
<div class="t5-menus">
	<div class="t5-menu-img-c">
		<img src="<?=wp_get_attachment_url($menu_logo);?>">
	</div>
	<div class="t5-menus-btn-c">
		<ul>
		<?php

			$pages = get_post_meta(
				get_post_meta( 
					get_the_ID(), 'floating_menu_selected', true), 
					'floating_menu_items',
					true
				);

				

			foreach($pages as $page):?>
				<?php 
					$is_active = "";
					$link = get_permalink($page['pages']);
					if(get_permalink() == get_permalink($page['pages']))
						$is_active = "active";
					
				?>
				<li class="<?= $is_active ?>"><a href="<?= $link ?>"><?= $page['name'] ?> </a></li>
				
			<?php endforeach;?>
		</ul>
	</div>
</div>

<?php elseif($type == 'no_logo'):?>
<ul class="nav nav-tabs">
	<?php
	$pages = get_post_meta(
		get_post_meta( 
			get_the_ID(), 'floating_menu_selected', true), 
			'floating_menu_items',
			true
		);

		

	foreach($pages as $page):?>
		<?php 
			$style='';
			$link = get_permalink($page['pages']);
			$is_active = "";
			if(get_permalink() == get_permalink($page['pages']))
				$is_active = "active";
			if($page['disable_menu_item'] == 1){
				$link = '';
				$style = 'cursor:default;';

			}
				
			
		?>
		<li class="<?= $is_active ?>"><a style="<?= $style; ?>" href="<?= $link ?>"><?= $page['name'] ?> </a></li>
		
	<?php endforeach;?>
</ul>

<?php elseif($type == 'top_logo'):?>
<div class="text-center floating-logo-top">
		<img src="<?=wp_get_attachment_url($menu_logo);?>">
</div>

<ul class="nav nav-v3">
	<?php
	$pages = get_post_meta(
		get_post_meta( 
			get_the_ID(), 'floating_menu_selected', true), 
			'floating_menu_items',
			true
		);

		

	foreach($pages as $page):?>
		<?php 
			$style='';
			if($page['custom_menu_link'] == 1 || $page['custom_menu_link'] == '1'){
				$link = '#'.$page['custom_link'];
			} else {
				$link = get_permalink($page['pages']);
			}
			$is_active = "";
			if(get_permalink() == get_permalink($page['pages']))
				$is_active = "active";
			if($page['disable_menu_item'] == 1){
				$link = '';
				$style = 'cursor:default;';

			}
				
			
		?>
		<li class="<?= $is_active ?>"><a style="<?= $style; ?>" href="<?= $link ?>"><?= $page['name'] ?> </a></li>
		
	<?php endforeach;?>
</ul>
<?php endif;?>
</div>

