<?php get_header(); ?>
	<main role="main">
		<section>
		<form action="" id="searchform" method="get"><div id="main_search_form" class="input-group">
            <input type="text" class="form-control" placeholder="Cerca" name="s" value="<?= $_GET['s'];?>">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
            </div>
        </div></form>
		
			<?php get_template_part('loop'); ?>
		</section>
	</main>
<?php get_footer(); ?>
