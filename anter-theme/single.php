<?php get_header(); ?>
<script>
window.twttr = (function (d,s,id) {
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
    js.src="//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
    }(document, "script", "twitter-wjs"));

$(document).on('click','.twitter',function(){

   var tweetUrl = 'https://twitter.com/intent/tweet?url='+window.location.href;
   var x = screen.width/2 - 700/2;
    var y = screen.height/2 - 450/2;
    var child = window.open(tweetUrl, "popupWindow", "width=600, height=400,left="+x+",top="+y);
       child.focus();
      var timer = setInterval(checkChild, 1);
      document.domain = 'example.com';     //Replace it with your domain
      function checkChild() {
        if(child.closed){
          clearInterval(timer);
        }
        if(child.window.closed){   //Code in this condition will execute after successfull post
          console.log("x");
          dataLayer.push({
            'event': "socialShare",
            'socialNetwork': "twitter", //variable
            'socialAction': 'share',
            'socialTarget': tweetUrl //variable
            });
          clearInterval(timer);
        }  
      }
});
</script>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <?php if ( has_post_thumbnail() && get_post_meta( $post->ID ,'enable_image_header' , true) == 'yes') : // Check if Thumbnail exists ?>
	

<section id="blur-bg" class="text-center">
        <div class="article-hero-slider"><img src=""></div>
        <div class="article-hero-img-cont">
        <div class="article-hero-img text-center container">
            <?= the_post_thumbnail( 'full',array('id'=>'srouce-image-hero','class'=>'feat-img') ); ?>
        </div>
    </div>
</section>
<?php endif; ?>
	<section id="article-main-container">
        <div class="container">
            <div class="article-view clearfix">
                <div class="share-links">
                <?php 
                        $ch = curl_init();
                        $optArray = array(
                            CURLOPT_URL => 'https://graph.facebook.com/?id='.get_permalink(),
                            CURLOPT_RETURNTRANSFER => true
                        );
                        curl_setopt_array($ch, $optArray);
                        $result = curl_exec($ch);
                        $result = json_decode( $result );
                        ?>
                    <span class="share-count-bold"><?= $result->share->share_count ?></span>
                    <p class="share-count-label">condivisioni</p>
                    <div class="social-media-share-button">
                        
                        <div class="share-buttons"><a href="<?= 'https://facebook.com/sharer/sharer.php?u='.urlencode(get_permalink()) ?>" 
                        onclick="window.open('<?= 'https://facebook.com/sharer/sharer.php?u='.urlencode(get_permalink()) ?>','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"
                        ><img src="<?php echo get_bloginfo( 'template_directory' );?>/img/fb.png"></a></div>
                       
                        <div class="share-buttons"><a href="javascript:void(0)" class="twitter">
                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/tw.png"></a></div>
                        <div class="share-buttons"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=urlencode(get_permalink()) ?>"><img width="100%" src="<?php echo get_bloginfo( 'template_directory' );?>/img/in-1.png"></a></div>
                    </div>
                </div>
                <div class="post-contents">
                    <div class="post-header">
                        <p class="post-date"><?php the_time('j F Y'); ?> | <?php _e( 'Published by', 'anter_translation' ); ?> <?= get_the_author(); ?></p>
                        <h1 class="post-title"><?php the_title(); ?></h1>
						<?php $subtitle = get_post_meta(get_the_ID(), 'sub_title', true);?>
                        <p class="post-short-desc"><?php if(isset($subtitle)) echo $subtitle; ?></p>
                        <?php do_action('print_tags');?>
                    </div>
                    <div class="article-content">
                        <p><?php echo the_content();?></p>
                    </div>
                </div>
            </div>

<!--             
			<div class="container">
                <div class="center-hr chrv2">
                	<span class="center-hr-element">
	                    <div class="commenti"><p>commenti</p></div>
                	</span>
                </div>
            </div>
           
            
            <div class="container text-center">
                <div class="fb-comments" data-href="<?= get_permalink(); ?>" data-numposts="5"></div>
            </div> -->
            <div class="container">
               	<div class="center-hr chrv2">
	         	   <span class="center-hr-element">
                      	<div class="commenti"><p>CONTINUA CON</p></div>
                   	</span>
                </div>
            </div>

			


    

            <div class="row row-eq-height class rel-article">















<?php
$related_post = array(
    get_post_meta( $post->ID, 'rp_1', true ),
    get_post_meta( $post->ID, 'rp_2', true ),
    get_post_meta( $post->ID, 'rp_3', true )
);
$related_post_a = $related_post;

$rem = 3;

foreach($related_post as $key=>$rel)    
    if(!$rel)
        unset($related_post[$key]);
    else
        $rem -= 1;

$orig_post = $post; 

global $post;



if(count($related_post) > 0):
    $args = array(
        'post__not_in' => array($post->ID),
        'post__in'      => $related_post,
        'post_type' => array('post','page'),
		'posts_per_page'=> count($related_post)
		
    );


$my_query = new wp_query( $args );  
$rem = $rem - $news_2->my_query;
if($rem != 3)
while( $my_query->have_posts() ) {
    $my_query->the_post();
    
    
?>

        
        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd">
            <div class="card-container">
                <div class="card-img-container">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumb-img',array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
                    </a>
                <?php else:?>
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                        </a>
                <?php endif; ?>
                    
                    <div>
                        <?php 
                        
                           $category_link = get_category_link(get_the_category()[0]->term_id );
                           ?>
                        <a href="<?= $category_link ?>"><p class="card-img-text"><?= (isset(get_the_category()[0]->cat_name)) ? get_the_category()[0]->cat_name : 'pagina'; ?></p></a>

                    </div>
                </div>
                <div class="card-content">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
                    <p class="card-text">
                        <?php echo(get_the_excerpt()); ?>
                    </p>
                </div>
            </div>
        </div>

    
    

<?php }
$post = $orig_post;
wp_reset_query();
endif;

$orig_post = $post;
global $post;
$related_post[] = $post->ID;
$tags = wp_get_post_tags($post->ID);


$args=array(
    'post__not_in' => $related_post,
    'posts_per_page'=>$rem, // Number of related posts to display.
    'ignore_sticky_posts'=>1
);

if(get_the_category()[0]->count > 1)
    $args['category_name'] = get_the_category()[0]->name;


$my_query = new wp_query( $args );  

if($rem != 0)
while( $my_query->have_posts() ) {
    $my_query->the_post();
    $related_post[] = $post->ID;
    
?>

        
        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd">
            <div class="card-container">
                <div class="card-img-container">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumb-img',array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
                    </a>
                    <?php else:?>
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                        </a>
                <?php endif; ?>
                    
                    <div>
                        <?php 
                        
                            $category_link = get_category_link(get_the_category()[0]->term_id );
                            ?>
                        <a href="<?= $category_link ?>"><p class="card-img-text"><?= get_the_category()[0]->cat_name; ?></p></a>

                    </div>
                </div>
                <div class="card-content">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
                    <p class="card-text">
                        <?php echo(get_the_excerpt()); ?>
                    </p>
                </div>
            </div>
        </div>

    
        

<?php 
$rem--;
}

$post = $orig_post;
wp_reset_query();


$orig_post = $post;
global $post;
$related_post[] = $post->ID;
$tags = wp_get_post_tags($post->ID);



$args=array(
    'post__not_in' => $related_post,
    'posts_per_page'=>$rem, // Number of related posts to display.
    'ignore_sticky_posts'=>1
);


$my_query = new wp_query( $args );  

if($rem != 0)
while( $my_query->have_posts() ) {
    $my_query->the_post();
    
    
?>

        
        <div class="col-xs-12 col-sm-6 col-md-4 remove-padd">
            <div class="card-container">
                <div class="card-img-container">
                <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumb-img',array('class' => 'img/1img.jpg')); // Declare pixel size you need inside the array ?>
                    </a>
                    <?php else:?>
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/default.jpg">
                        </a>
                <?php endif; ?>
                    
                    <div>
                        <?php 
                        
                            $category_link = get_category_link(get_the_category()[0]->term_id );
                            ?>
                        <a href="<?= $category_link ?>"><p class="card-img-text"><?= get_the_category()[0]->cat_name; ?></p></a>

                    </div>
                </div>
                <div class="card-content">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3 class="card-heading"><?php the_title(); ?></h3></a>
                    <p class="card-text">
                        <?php echo(get_the_excerpt()); ?>
                    </p>
                </div>
            </div>
        </div>

    
    

<?php }

$post = $orig_post;
wp_reset_query();
?>
    

    









            
            
                
 
            </div>
        </div>
    </section>



	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

<?php get_footer(); ?>
