<?php 
// This file is for ajax functions
function load_ajax_video_gallery(){
    if(!isset($_GET['post'])){
        return;
    }
    $counter = 1;   
    $data = get_post_meta($_GET['post'], 'video_gallery_data', true);
    $title = get_post_meta($_GET['postid'],'video_gallery_title',true);
    $limit = get_post_meta($_GET['post'], 'video_gallery_item_per_page', true);
    $youtube_url = get_post_meta($_GET['post'], 'featured_video', true);
    $start =  (int)$_GET['page_num'] * $limit;    
    $end =  (int)$_GET['page_num'] * $limit * 2;    
    if($end > sizeof($data)){
        $end = sizeof($data);
    }
    for($in = $start ; $in < $end ; $in++ ){
        ?>

        <div class="eco-mag-img-container square video-thumb">	    
	    	    <?php if ($data[$in]): ?>
	                <?php preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $data[$in]['url'], $matches);?>
	    	        <img class="hide-img" src="<?='https://img.youtube.com/vi/' . $matches[0] . '/maxresdefault.jpg'?>">
                    <input class="youtube_url" type="hidden" name="youtube_url" value=<?= $matches[0] ?>>
	    	    <?php endif;?>
                    <div class="play_container"><img src="<?=get_bloginfo('template_directory');?>/img/play_button.png"></div>
                    <div class="square-content-overlay"></div>
                    <div class="square-content">
                        <p class="gallery-card-date"></p>
                        <p class="gallery-card-text"><?= $data[$in]['name'] ?></p>
                    </div>
        </div>

        <?php
        
        if($counter == $limit) 
            break;
        $counter++;
    }
    die();
}

add_action( 'wp_ajax_load_next_vid_set', 'load_ajax_video_gallery' );
add_action( 'wp_ajax_nopriv_load_next_vid_set', 'load_ajax_video_gallery' );



function load_ajax_photo_gallery(){
    if(!isset($_GET['post'])){
        return;
    }
    $counter = 1;   
    $images_id = get_post_meta($_GET['post'],'vdw_gallery_id',true);
    $limit = get_post_meta($_GET['post'],'photo_per_page',true);
    
    $gallery = array();
    $start =  (int)$_GET['page_num'] * $limit;    
    $end =  (int)$_GET['page_num'] * $limit * 2; 
    $id_counter = (int)$_GET['page_num'];
    foreach($images_id as $image_id) {
        $id_counter++;
        $gallery[] = array(
            "id" => $id_counter,
            "uploaded" => human_time_diff( get_post_time( 'U',  $image_id ), current_time( 'timestamp' ) ).' '.__( 'ago' ),
            "url" => wp_get_attachment_url( $image_id),
            "thumb"=>  $image_id,
            "title"=>get_the_title($image_id)
        );
    };
    
   
    if($end > sizeof($gallery)){
        $end = sizeof($gallery);
    }
    for($in = $start ; $in < $end ; $in++ ){
        ?>
            
            <div class="eco-mag-img-container square image-modal-toggle">
            <img data-id=<?= $gallery[$in]['id'] ?> class="hide-img" src="<?= $gallery[$in]['url']?>">

             <?= wp_get_attachment_image( 
                    $gallery[$in]['thumb'],
                    'photo-thumb', "",
                    array(
                        "class" => "hide-img",
                        "data-id" => $gallery[$in]['id'],
                        "asrc"=>$gallery[$in]['url']
                         )
                    );?>

            <div class="square-content-overlay"></div>
            <div class="square-content">
                <!-- <p class="gallery-card-date"><?= $gallery[$in]['uploaded']?></p>
                <p class="gallery-card-text"><?= $gallery[$in]['title']?></p> -->
            </div>
            </div>


        <?php
        
        if($counter == $limit) 
            break;
        $counter++;
    }
    die();
}

add_action( 'wp_ajax_load_photo_gallery', 'load_ajax_photo_gallery' );
add_action( 'wp_ajax_nopriv_load_photo_gallery', 'load_ajax_photo_gallery' );



